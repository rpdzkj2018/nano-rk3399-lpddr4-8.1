package teaonly.rk.droidipcam;

interface ICameraCrashService
{
    boolean prepareMedia();
	void stopRecording();
	boolean startRecording();
	int getRecordState();
	void startPreview();
	void stopPreview() ;
	void pip(int width, int heigth);
}
package teaonly.rk.droidipcam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import teaonly.rk.droidipcam.CameraManager.CameraProxy;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.os.Environment;
import android.os.RemoteException;
import android.os.StrictMode.VmPolicy;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.view.MotionEvent;

@SuppressLint("InlinedApi")
public class FloatView  extends View {
    public static final String ACTION_PIP_SIZE = "com.android.rk.pip.sizechange"; //PIP SIZE
    public static final String ACTION_PIP_EXIT = "com.android.rk.pip.exit"; //PIP SIZE
    private  WindowManager mWm;
	private  WindowManager.LayoutParams wmParams;
	private CameraProxy mCamera;
	private SurfaceHolder mSurfaceHolder;
	static final int DOUBLE_CLICK_TIME_SPACE = 2000; // 双击时间间隔 
	
//	private LinearLayout lay; // 愿揽控件�?
	LinearLayout mRootView;
	public void setCamera(CameraProxy mCamera, FloatViewCallBack callback) {
		this.mCamera = mCamera;
		this.callback = callback;
		/*DisplayMetrics dm = new DisplayMetrics();
        dm = getResources().getDisplayMetrics(); 
        height = dm.heightPixels;
        width = dm.widthPixels;*/
	}
	private SurfaceView mPreview;
	private Context context;
	private ToneGenerator mTone;
	private int height;
	private int width;
	private FloatViewCallBack callback;
	private boolean minScreen = true;
	private keyEventReceiver receiver;

	public FloatView(Context context) {
        super(context);
        
        this.context = context;
     // 设置悬浮窗体属�?
        // 1.得到WindoeManager对象�?
        mWm = (WindowManager) context.getSystemService("window");
        receiver = new keyEventReceiver();
        registerBoradcastReceiver();
    }

	public SurfaceHolder getHolder() {
		return mSurfaceHolder;
	}

	public void floatView(int w,int h) {
		// 2.得到WindowManager.LayoutParams对象，为后续设置相关参数做准备：
		wmParams = new WindowManager.LayoutParams();
		// 3.设置相关的窗口布�?��数，要实现悬浮窗口效果，要需要设置的参数�?
		// 3.1设置window type
		wmParams.type = LayoutParams.TYPE_PHONE;
		// 3.2设置图片格式，效果为背景透明 //wmParams.format = PixelFormat.RGBA_8888;
		//wmParams.format = 1;
		wmParams.format=PixelFormat.OPAQUE;
		wmParams.flags|=8;

		// 下面的flags属�?的效果形同�?锁定”�? 悬浮窗不可触摸，不接受任何事�?同时不影响后面的事件响应�?
		//wmParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL | LayoutParams.FLAG_NOT_FOCUSABLE;
		//wmParams.flags |=WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
		wmParams.flags = wmParams.flags | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS; // 排版不受限制  
		wmParams.windowAnimations = android.R.style.Animation_Translucent;

		// 4.// 设置悬浮窗口长宽数据
		wmParams.width = LayoutParams.WRAP_CONTENT;
		wmParams.height = LayoutParams.WRAP_CONTENT;
		// 5. 调整悬浮窗口至中�?
		wmParams.gravity = Gravity.LEFT | Gravity.TOP;
		// 6. 以屏幕左上角为原点，设置x、y初始�?
		wmParams.x = 0;
		wmParams.y = 0;
		// 7.将需要加到悬浮窗口中的View加入到窗口中了：
        // 如果view没有被加入到某个父组件中，则加入WindowManager�?
        mPreview = new SurfaceView(context);
        mSurfaceHolder  = mPreview.getHolder();
        LayoutParams params_sur = new LayoutParams();
        width = w;
        height = h;
        params_sur.width = w;
        params_sur.height = h;
        params_sur.alpha = 256;
        mPreview.setLayoutParams(params_sur);
        CameraHolder.instance().setHolder(mPreview.getHolder(), mPreview);
        mPreview.getHolder()
                .setType(mSurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mPreview.getHolder().addCallback(new CameraPreviewCallback());
		
		mRootView = new LinearLayout(context);
		LayoutParams params_rel = new LayoutParams();
		params_rel.width = LayoutParams.WRAP_CONTENT;
		params_rel.height = LayoutParams.WRAP_CONTENT;
		mRootView.setLayoutParams(params_rel);
		mRootView.setBackgroundResource(R.drawable.editor_btn);
		mRootView.addView(mPreview);
		mPreview.requestFocus();
		mPreview.setFocusable(true);
		mPreview.setFocusableInTouchMode(true);
		mPreview.setSelected(true);
		mRootView.requestFocus();
		mRootView.setFocusable(true);
		mRootView.setFocusableInTouchMode(true);
		mRootView.setSelected(true);
		mRootView.setOnClickListener(viewClickLister);
                mRootView.setOnTouchListener(mTouchListener);
		mWm.addView(mRootView, wmParams); // 创建View
		
		
		/*DisplayMetrics dm = new DisplayMetrics();
        dm = getResources().getDisplayMetrics(); 
        height = dm.heightPixels;
        width = dm.widthPixels;*/
		/*Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			int index = 0;

			@Override
			public void run() {
				// 第一个参数设置为null,就可以静音拍照了
				mCamera.takePicture(shutterCallback, null, jpegCallback);
			}
		}, 1000); // 延迟1秒拍�?*/	
		}

	private OnClickListener viewClickLister = new OnClickListener() {
        
        @Override
        public void onClick(View v) {
            Log.i("FloatView", "View Click!");
            updateView();
        }
    };

        private int mCurrentX = 0;
        private int mCurrentY = 0;
        private int mStartX = 0;
        private int mStartY = 0;
private boolean isDoubleClick = false;
        private OnTouchListener mTouchListener = new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                                isDoubleClick = false;
                                break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                                isDoubleClick = true;
                                break;
                        case MotionEvent.ACTION_MOVE:
                                break;
                        case MotionEvent.ACTION_POINTER_UP:
                                break;
                        case MotionEvent.ACTION_UP:
                                break;
                        }
                        if (!isDoubleClick) {
                                tarckFlinger(event);
                                return true;
                        }else {
                          Log.i("FloatView", "View Click!");
                           updateView();
                        }
                        return true;
                }
        };

private boolean tarckFlinger(MotionEvent event) {
                /*
                 * wmParams.width+=50; wmParams.height+=50;
                 * mVideoView.setVideoMeasure(wmParams.width, wmParams.height);
                 * wm.updateViewLayout(mRootView, wmParams);
                 */
                mCurrentX = (int) event.getRawX();
                mCurrentY = (int) event.getRawY();
                switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                        mStartX = (int) event.getX();
                        mStartY = (int) event.getY();
                        break;
                case MotionEvent.ACTION_MOVE:
                        updateWindowParams();
                        break;
                case MotionEvent.ACTION_UP:
                        mStartX = 0;
                        mStartY = 0;
                        break;
                }
                return true;
        }

        private void updateWindowParams() {
                wmParams.x = mCurrentX - mStartX;
                wmParams.y = mCurrentY - mStartY;
                mWm.updateViewLayout(mRootView, wmParams);
        }

    public void registerBoradcastReceiver(){  
        IntentFilter myIntentFilter = new IntentFilter();  
        myIntentFilter.addAction(ACTION_PIP_SIZE);  
        myIntentFilter.addAction(ACTION_PIP_EXIT);  
        //注册广播        
        context.registerReceiver(receiver, myIntentFilter);  
    }  
    class keyEventReceiver extends BroadcastReceiver {

       

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_PIP_SIZE)) {
                Log.i("FloatView", "------------------------------------"+ACTION_PIP_SIZE);
                intent.getBooleanExtra("", false);
                intent.getIntExtra("", -1);
                updateView();
            }
            else if(action.equals(ACTION_PIP_EXIT)){
                Log.i("FloatView", "------------------------------------"+ACTION_PIP_EXIT);
                callback.stopSvr();
            }
        }
    }

    private void updateView(){
        if(minScreen){
        mPreview.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        //wmParams.flags =wmParams.flags|WindowManager.LayoutParams.FLAG_FULLSCREEN;
        wmParams.width = LayoutParams.MATCH_PARENT;
        wmParams.height = LayoutParams.MATCH_PARENT;
        minScreen = false;
        }
        else{
            mPreview.setLayoutParams(new LinearLayout.LayoutParams(width, height));
         // 4.// 设置悬浮窗口长宽数据
            wmParams.width = LayoutParams.WRAP_CONTENT;
            wmParams.height = LayoutParams.WRAP_CONTENT;
            // 5. 调整悬浮窗口至中�?
            wmParams.gravity = Gravity.LEFT | Gravity.BOTTOM;
            // 6. 以屏幕左上角为原点，设置x、y初始�?
            wmParams.x = 0;
            wmParams.y = 0;
            minScreen = true;
        }
	    mWm.updateViewLayout(mRootView, wmParams);  
	    }  
	// 照相机的快门监听,�?��用来播放拍照声音
	private ShutterCallback shutterCallback = new ShutterCallback() {
		public void onShutter() {
			if (mTone == null) {
				mTone = new ToneGenerator(AudioManager.STREAM_MUSIC,
						ToneGenerator.MAX_VOLUME);
			}
			mTone.startTone(ToneGenerator.TONE_PROP_BEEP2);
		}
	};
	// 得到jpeg图片并且保存
	private PictureCallback jpegCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
			Parameters ps = camera.getParameters();
			if (ps.getPictureFormat() == PixelFormat.JPEG) {
				String path = save(data);
				Uri uri = Uri.fromFile(new File(path));
				// Intent intent = new Intent();
				// intent.setAction("android.intent.action.VIEW");
				// intent.setDataAndType(uri, "image/jpeg");
				// startActivity(intent);
			}
		}
	};

	// 设置保存地址
	private String save(byte[] data) {
		String saveDir = Environment.getExternalStorageDirectory().toString()
				+ "/backstageCamera/image/";
		String fileName = System.currentTimeMillis() + ".jpg";
		try {
			File dirFile = new File(saveDir);
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}
			File file = new File(saveDir, fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(file, true);
			fos.write(data);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return saveDir + fileName;
	}
	
	
	 public void onDelete(){
	     if(null != mRootView){
	    	if(mRootView.getParent()!=null){
	    		System.out.println("onDelete and stop the service");
	    		mRootView.removeAllViews();
	    		mWm.removeView(mRootView);
	    		mRootView = null;
	    		context.unregisterReceiver(receiver);
	    		//mContext.stopService(new Intent(mContext,MediaFloatService.class));
	    	}   	
	     }
	    }
	// 预览界面CameraPreview
	@TargetApi(9)
	class CameraPreviewCallback  implements SurfaceHolder.Callback {
		@Override
		public void surfaceChanged(SurfaceHolder holder, int arg1, int arg2,
				int arg3) {
			System.out.println("*****surfaceChanged");
			mSurfaceHolder = holder;

		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			System.out.println("*****surfaceCreated");
			mSurfaceHolder = holder;
			Log.i("process", Thread.currentThread().getName());

		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			System.out.println("*****surfaceDestroyed");
			mPreview = null;
			mSurfaceHolder = null;
		}
	}
}

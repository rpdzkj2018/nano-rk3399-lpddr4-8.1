package teaonly.rk.droidipcam;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class PIPBroadCastReceiver extends BroadcastReceiver {

	public static final String ACTION_MOVIE_START = "com.rk.pip";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
	if (action.equals(ACTION_MOVIE_START)) {
	        Log.i("bc", "------------------------------------------------");
			Intent mIntent = new Intent("pip");
			mIntent.putExtra("width", intent.getIntExtra("width", 400));
			mIntent.putExtra("height", intent.getIntExtra("height", 300));
			mIntent.setClass(context, CameraCrashService.class);
			context.startService(mIntent);
		}
	}

}

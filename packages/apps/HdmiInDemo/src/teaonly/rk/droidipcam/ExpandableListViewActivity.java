package teaonly.rk.droidipcam;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import teaonly.rk.droidipcam.FileListViewAdapter.SuperTreeNode;
import teaonly.rk.droidipcam.R;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Toast;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ExpandableListView;

public class ExpandableListViewActivity extends Activity {
	
	final static String TAG = "ExpandableListViewActivity";
	final static boolean DEBUG = false;	//true;
	//static private final boolean DEBUG = false;

	private static void LOG(String str)
	{
		if(DEBUG)
		{
			Log.d(TAG,str);
		}
	}
	
	
	public boolean openwiththread = false;
	public ProgressDialog openingDialog = null;
	private Context mContext;
	private CameraUtil mUtil = new CameraUtil();
	public boolean is_last_path = false;
	private FileListViewAdapter mExpandableListAdapter;
	private ExpandableListView mExpandableListView;
	private int mGroupPosition = -1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expandablelistview);
		mExpandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
		mExpandableListAdapter = new FileListViewAdapter(this);
		mContext = this;
		mExpandableListView.setAdapter(mExpandableListAdapter);
		mExpandableListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				List<SuperTreeNode> getTreeNode = mExpandableListAdapter
						.GetTreeNode();
				if (getTreeNode != null && getTreeNode.size() > 0) {
					FileInfo mediaDir = getTreeNode.get(groupPosition).parent;
					is_last_path = true;
					mGroupPosition = groupPosition;
					openDir(mediaDir.file.getAbsolutePath());
					/*String str = "parent_id = " + groupPosition
							+ " child_id = ";
					Toast.makeText(ExpandableListViewActivity.this, str,
							Toast.LENGTH_SHORT).show();*/
				}
				return false;
			}
		});
	   mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){
				@Override
				public boolean onChildClick(ExpandableListView arg0, View arg1,
						int groupPosition, int childPosition, long arg4) {
					List<SuperTreeNode> getTreeNode = mExpandableListAdapter.GetTreeNode();
					if(getTreeNode != null &&  getTreeNode.size() > 0){
						 List<FileInfo> childs = getTreeNode.get(groupPosition).childs;
						 FileInfo mediaDir = childs.get(childPosition);
						 Intent intent = new Intent();
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.setAction(android.content.Intent.ACTION_VIEW);
							intent.setDataAndType(Uri.fromFile(mediaDir.file),
									mediaDir.file_type);

							try {
								startActivity(intent);
							} catch (android.content.ActivityNotFoundException e) {
								Toast.makeText(ExpandableListViewActivity.this,
										getString(R.string.noapp), Toast.LENGTH_SHORT)
										.show();
								Log.e(TAG, "Couldn't launch music browser", e);
							}
					}
					return true;
				}
				
			});
	}
	public void onResume(){
		super.onResume();
		mGroupPosition = -1;
		File mediaDir = CameraUtil.getMediaDir();
		if(mediaDir != null)
			openDir(mediaDir.getAbsolutePath());
	}
	public void openDir(String path){
    	fill_path = path;
    	mFillHandler.removeCallbacks(mFillRun);	//�Ƚ���
    	CameraUtil.is_enable_fill = false;		//�Ƚ���֮ǰfill��thread    	
    	CameraUtil.is_finish_fill = false;

    	/**/
    	File files = new File(path); 
	if(!files.exists() || !files.canRead()){
		CameraUtil.is_enable_fill = true;
		CameraUtil.is_finish_fill = true;
		return;
	}
    	long file_count = files.listFiles().length; 
    	LOG("in the openDir, file_count = " + file_count);
    	if(file_count > 1500){
    		openwiththread = true;
	    	if(openingDialog == null){
				openingDialog = new ProgressDialog(ExpandableListViewActivity.this);
				openingDialog.setTitle(R.string.str_openingtitle);
				openingDialog.setMessage(getString(R.string.str_openingcontext));
				openingDialog.show();
			}else{
				openingDialog.show();
			}
    	}else{    	
    		openwiththread = false;
    	}
    	mOpenHandler.postDelayed(mOpeningRun, 200);	
    	mFillHandler.postDelayed(mFillRun, 300);	
    }
	public String fill_path = null;
	Handler mOpenHandler = new Handler();	
	Runnable mOpeningRun = new Runnable() {
		public void run() { 
			if(openwiththread){
				mUtil.refillwithThread(mContext,fill_path);	
			}else{
				mUtil.refill(mContext,fill_path);				
			}
			mOpenHandler.removeCallbacks(mOpeningRun);
		}
	};

	Handler mFillHandler = new Handler();	
	Runnable mFillRun = new Runnable() {
		public void run() { 
			LOG("in the mFillRun, is_finish_fill = " + CameraUtil.is_finish_fill);
			LOG("in the mFillRun, adapte size = " + mUtil.folder_array.size());
			if(!CameraUtil.is_finish_fill){		
				if(openwiththread){
					setFileAdapter(false, false);
					//main_ListView.setSelection(mFileControl.folder_array.size() - 6);
				}
				mFillHandler.postDelayed(mFillRun, 1500); 				
			}else{
				if(openwiththread)
					setFileAdapter(false, false);
				else {
					if (is_last_path) {
						is_last_path = false;
						setFileAdapter(false, false);
					} else {
						setFileAdapter(true, true);
					}
				}
				mFillHandler.removeCallbacks(mFillRun);
				if(openingDialog != null)
					openingDialog.dismiss();
			}			
		}
	};
	 public void setFileAdapter(boolean is_animation, boolean is_left){
	    	/**/
	    	//tempAdapter = new NormalListAdapter(this, mFileControl.get_folder_array());
		
	    	//tempAdapter = new NormalListAdapter(this, folder_array);
	    	//main_ListView.setAdapter(tempAdapter);
	    	List<FileListViewAdapter.SuperTreeNode> superNodeTree = mExpandableListAdapter
					.GetTreeNode();
	    	ArrayList<FileInfo> folder_array = mUtil.folder_array;
	    	if(superNodeTree.size() <= 0){
		    	 for (int i = 0; i < folder_array.size(); i++) {
		    		 FileListViewAdapter.SuperTreeNode node = new FileListViewAdapter.SuperTreeNode();
						node.parent = (FileInfo)folder_array.get(i);
						if(!node.parent.isDir){
							
						}
						/*for (int j = 0; j < childs[i].length; j++) {
							node.childs.add(childs[i][j]);
						}
						treeNode.add(node);*/
						superNodeTree.add(node);
		    	 }
	    	 }else{
	    		 if(mGroupPosition >= 0){
	    			 SuperTreeNode superTreeNode = superNodeTree.get(mGroupPosition);
	    			/* for (int i = 0; i < folder_array.size(); i++) {
	    				 superTreeNode.childs.folder_array.get(i));
	    			 }*/
	    		     if(folder_array.size() > 0){
	    		    	 superTreeNode.childs = folder_array;
	    		     }else
	    		    	 superTreeNode.childs.clear();
	    			
	    		 }
	    	 }
	    	 mExpandableListAdapter.UpdateTreeNode(superNodeTree);
	    	 mExpandableListAdapter.notifyDataSetChanged();
	    	if(is_animation){
	    		int dir = 0;
	    		if(is_left)
	    			dir = 200;
	    		else
	    			dir = -200;
		    	TranslateAnimation myAnimation_Translate = new TranslateAnimation(dir, 0.0f, 0.0f, 0.0f);
		    	myAnimation_Translate.setDuration(300); 
		    	mExpandableListView.setAnimation(myAnimation_Translate);
	    	}
	    	//setMyTitle(mFileControl.get_currently_path());
	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.expandablelistview1, menu);
		
		return true;
	}
	OnChildClickListener stvClickEvent = new OnChildClickListener() {
		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			String msg = "parent_id = " + groupPosition + " child_id = "
					+ childPosition;
			Toast.makeText(ExpandableListViewActivity.this, msg,
					Toast.LENGTH_SHORT).show();
			return false;
		}
	};
}

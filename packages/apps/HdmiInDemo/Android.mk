LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_JAVA_LIBRARIES := bouncycastle conscrypt 
LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4 android-support-v13 jsr305
LOCAL_STATIC_JAVA_LIBRARIES += xmp_toolkit

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
        $(call all-java-files-under, src) \
		src/teaonly/rk/droidipcam/ICameraCrashService.aidl

LOCAL_PACKAGE_NAME := droidipcam

LOCAL_CERTIFICATE := platform
LOCAL_PROGUARD_FLAG_FILES := proguard.flags


#LOCAL_DEX_PREOPT := false
include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))

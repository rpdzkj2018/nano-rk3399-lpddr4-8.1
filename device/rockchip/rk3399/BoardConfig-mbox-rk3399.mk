# BoardConfig.mk文件命令格式：BoardConfig-xxx-xxx.mk
# 例如：BoardConfig-pro-rk3288.mk

# ARCH
export ARCH=arm64

# DEVICE NAME
export TARGET_DEVICE=rk3399
export TARGET_PRODUCT=rk3399

# PRODUCT NAME
export PRODUCT_NAME=rk3399

# LUNCH
export TARGET_LUNCH=rk3399-userdebug

# SYSTEM
export TARGET_SYSTEM=android8.1

# UBOOT DEFCONFIG
export UBOOT_DEFCONFIG=rk3399_defconfig

# KERNEL DEFCONFIG
export KERNEL_DEFCONFIG=rockchip_defconfig

# DTS
export TARGET_BUILD_DTB=mbox-rk3399
export DTS_DIR=kernel/arch/arm64/boot/dts/rockchip/$TARGET_BUILD_DTB.dts

# UBOOT BUILD WITH MKV*.SH
export BUILD_UBOOT_WITH_MKV=true

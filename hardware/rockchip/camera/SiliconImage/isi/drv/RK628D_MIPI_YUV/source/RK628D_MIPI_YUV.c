/******************************************************************************
 *
 * The copyright in this software is owned by Rockchip and/or its licensors.
 * This software is made available subject to the conditions of the license
 * terms to be determined and negotiated by Rockchip and you.
 * THIS SOFTWARE IS PROVIDED TO YOU ON AN "AS IS" BASIS and ROCKCHP AND/OR
 * ITS LICENSORS DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS WITH
 * RESPECT TO SUCH SOFTWARE, WHETHER EXPRESS,IMPLIED, STATUTORY OR OTHERWISE,
 * INCLUDING WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT,
 * MERCHANTABILITY, SATISFACTROY QUALITY, ACCURACY OR FITNESS FOR A PARTICULAR PURPOSE.
 * Except as expressively authorized by Rockchip and/or its licensors, you may not
 * (a) disclose, distribute, sell, sub-license, or transfer this software to any third party,
 * in whole or part; (b) modify this software, in whole or part; (c) decompile, reverse-engineer,
 * dissemble, or attempt to derive any source code from the software.
 *
 *****************************************************************************/
/**
 * @file RK628D.c
 *
 * @brief
 *   ADD_DESCRIPTION_HERE
 *
 *****************************************************************************/
#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>
#include <common/misc.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"

#include "RK628D_priv.h"


#define CC_OFFSET_SCALING  2.0f
#define I2C_COMPLIANT_STARTBIT 1U

#define RK628D_HDMI2MIPI 1

#define S_DDC5V (1 << 0)
#define S_SYNC  (1 << 7)

#define ERR_CHECK_RETURN(success, ret, reg, val) do {		\
	if (ret != success) {					\
		TRACE( RK628D_ERROR,				\
			"%s reg: %#x, value: %#x, result:%d\n",	\
			__FUNCTION__,				\
			reg,					\
			val,					\
			ret); 					\
		return 0;					\
	}							\
} while(0)

#define READ_REGISTER_TRACE(ret, reg, val) do { 	\
	TRACE( RK628D_DEBUG,				\
		"%s reg: %#x, value: %#x, result:%d\n",	\
		__FUNCTION__,				\
		reg,					\
		val,					\
		ret); 					\
} while(0)

/******************************************************************************
 * local macro definitions
 *****************************************************************************/
CREATE_TRACER( RK628D_INFO ,      "RK628D: ", INFO,          0U );
CREATE_TRACER( RK628D_WARN ,      "RK628D: ", WARNING,       0U );
CREATE_TRACER( RK628D_ERROR,      "RK628D: ", ERROR,         1U );
CREATE_TRACER( RK628D_DEBUG,      "RK628D: ", INFO,          0U );
CREATE_TRACER( RK628D_REG_INFO ,  "RK628D: ", INFO,          0U );
CREATE_TRACER( RK628D_REG_DEBUG,  "RK628D: ", INFO, 	     0U );

#define RK628D_SLAVE_ADDR       0xA0U                           /**< i2c slave address of the RK628D camera sensor */
#define RK628D_SLAVE_ADDR2      0xA0U
#define RK628D_SLAVE_AF_ADDR    0x00U                           /**< i2c slave address of the RK628D integrated AD5820 */

/******************************************************************************
 * local variable declarations
 *****************************************************************************/
const char RK628D_g_acName[] = "RK628D_SOC_PARREL";
extern const IsiRegDescription_t RK628D_g_aRegDescription[];
extern const IsiRegDescription_t RK628D_g_hdmi_input_check[];
extern const IsiRegDescription_t RK628D_g_aRegVedioON[];

extern const IsiRegDescription_t RK628D_RS1[];
extern const IsiRegDescription_t RK628D_RS2[];
extern const IsiRegDescription_t RK628D_RS3_30fps_interlace[];
extern const IsiRegDescription_t RK628D_RS3_60fps[];
extern const IsiRegDescription_t RK628D_RS5[];
extern const IsiRegDescription_t RK628D_RS6[];

const IsiSensorCaps_t RK628D_g_IsiSensorDefaultConfig;

#define RK628D_I2C_NR_REG_ADR_BYTES     (4U)
#define RK628D_I2C_NR_REG_DAT_BYTES     (4U)

static uint16_t g_suppoted_mipi_lanenum_type = SUPPORT_MIPI_FOUR_LANE;
#define DEFAULT_NUM_LANES SUPPORT_MIPI_FOUR_LANE

/******************************************************************************
 * local function prototypes
 *****************************************************************************/
static RESULT RK628D_IsiCreateSensorIss( IsiSensorInstanceConfig_t *pConfig );
static RESULT RK628D_IsiReleaseSensorIss( IsiSensorHandle_t handle );
static RESULT RK628D_IsiGetCapsIss( IsiSensorHandle_t handle, IsiSensorCaps_t *pIsiSensorCaps );
static RESULT RK628D_IsiSetupSensorIss( IsiSensorHandle_t handle, const IsiSensorConfig_t *pConfig );
static RESULT RK628D_IsiSensorSetStreamingIss( IsiSensorHandle_t handle, bool_t on );
static RESULT RK628D_IsiSensorSetPowerIss( IsiSensorHandle_t handle, bool_t on );
static RESULT RK628D_IsiCheckSensorConnectionIss( IsiSensorHandle_t handle );
static RESULT RK628D_IsiGetSensorRevisionIss( IsiSensorHandle_t handle, uint32_t *p_value);
static RESULT RK628D_IsiGetGainLimitsIss( IsiSensorHandle_t handle, float *pMinGain, float *pMaxGain);
static RESULT RK628D_IsiGetIntegrationTimeLimitsIss( IsiSensorHandle_t handle, float *pMinIntegrationTime, float *pMaxIntegrationTime );
static RESULT RK628D_IsiExposureControlIss( IsiSensorHandle_t handle, float NewGain, float NewIntegrationTime, uint8_t *pNumberOfFramesToSkip, float *pSetGain, float *pSetIntegrationTime );
static RESULT RK628D_IsiGetCurrentExposureIss( IsiSensorHandle_t handle, float *pSetGain, float *pSetIntegrationTime );
static RESULT RK628D_IsiGetAfpsInfoIss ( IsiSensorHandle_t handle, uint32_t Resolution, IsiAfpsInfo_t* pAfpsInfo);
static RESULT RK628D_IsiGetGainIss( IsiSensorHandle_t handle, float *pSetGain );
static RESULT RK628D_IsiGetGainIncrementIss( IsiSensorHandle_t handle, float *pIncr );
static RESULT RK628D_IsiSetGainIss( IsiSensorHandle_t handle, float NewGain, float *pSetGain );
static RESULT RK628D_IsiGetIntegrationTimeIss( IsiSensorHandle_t handle, float *pSetIntegrationTime );
static RESULT RK628D_IsiGetIntegrationTimeIncrementIss( IsiSensorHandle_t handle, float *pIncr );
static RESULT RK628D_IsiSetIntegrationTimeIss( IsiSensorHandle_t handle, float NewIntegrationTime, float *pSetIntegrationTime, uint8_t *pNumberOfFramesToSkip );
static RESULT RK628D_IsiGetResolutionIss( IsiSensorHandle_t handle, uint32_t *pSetResolution );
static RESULT RK628D_IsiRegReadIss( IsiSensorHandle_t handle, const uint32_t address, uint32_t *p_value );
static RESULT RK628D_IsiRegWriteIss( IsiSensorHandle_t handle, const uint32_t address, const uint32_t value );
static RESULT RK628D_IsiGetCalibKFactor( IsiSensorHandle_t handle, Isi1x1FloatMatrix_t **pIsiKFactor );
static RESULT RK628D_IsiGetCalibPcaMatrix( IsiSensorHandle_t   handle, Isi3x2FloatMatrix_t **pIsiPcaMatrix );
static RESULT RK628D_IsiGetCalibSvdMeanValue( IsiSensorHandle_t   handle, Isi3x1FloatMatrix_t **pIsiSvdMeanValue );
static RESULT RK628D_IsiGetCalibCenterLine( IsiSensorHandle_t   handle, IsiLine_t  **ptIsiCenterLine);
static RESULT RK628D_IsiGetCalibClipParam( IsiSensorHandle_t   handle, IsiAwbClipParm_t    **pIsiClipParam );
static RESULT RK628D_IsiGetCalibGlobalFadeParam( IsiSensorHandle_t       handle, IsiAwbGlobalFadeParm_t  **ptIsiGlobalFadeParam);
static RESULT RK628D_IsiGetCalibFadeParam( IsiSensorHandle_t   handle, IsiAwbFade2Parm_t   **ptIsiFadeParam);
static RESULT RK628D_IsiGetIlluProfile( IsiSensorHandle_t   handle, const uint32_t CieProfile, IsiIlluProfile_t **ptIsiIlluProfile );
static RESULT RK628D_IsiMdiInitMotoDriveMds( IsiSensorHandle_t handle );
static RESULT RK628D_IsiMdiSetupMotoDrive( IsiSensorHandle_t handle, uint32_t *pMaxStep );
static RESULT RK628D_IsiMdiFocusSet( IsiSensorHandle_t handle, const uint32_t Position );
static RESULT RK628D_IsiMdiFocusGet( IsiSensorHandle_t handle, uint32_t *pAbsStep );
static RESULT RK628D_IsiMdiFocusCalibrate( IsiSensorHandle_t handle );
static RESULT RK628D_IsiGetSensorMipiInfoIss( IsiSensorHandle_t handle, IsiSensorMipiInfo *ptIsiSensorMipiInfo);
static RESULT RK628D_IsiGetSensorIsiVersion(  IsiSensorHandle_t   handle, unsigned int* pVersion);

uint8_t GetNrDatBytesOfAllRegTable
(
    const uint32_t address
)
{
	return RK628D_I2C_NR_REG_DAT_BYTES;
#if 0
	#define CHECK_AND_RETURN(num) do {	\
		if (num > 0)			\
			return num;		\
	} while(0)

	uint8_t NrOfBytes = 0;

	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS1 );
	CHECK_AND_RETURN(NrOfBytes);
	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS2 );
	CHECK_AND_RETURN(NrOfBytes);
	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS3_60fps );
	CHECK_AND_RETURN(NrOfBytes);
	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS3_30fps_interlace );
	CHECK_AND_RETURN(NrOfBytes);
	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS5 );
	CHECK_AND_RETURN(NrOfBytes);
	NrOfBytes = IsiGetNrDatBytesIss( address, RK628D_RS6 );
	CHECK_AND_RETURN(NrOfBytes);

        TRACE( RK628D_ERROR, "%s Reg:%#x GetNrDatBytes Err!!!)\n", __FUNCTION__, address);
	return 0;
#endif
}


static RESULT RK628D_WriteRegArray
(
    IsiSensorHandle_t       handle,
    const IsiRegDescription_t *pRegDesc
)
{
	RESULT result = RET_SUCCESS;
	RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

	TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

	if ( pRK628DCtx == NULL )
	{
		TRACE( RK628D_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
		return ( RET_WRONG_HANDLE );
	}

	if ( pRegDesc == NULL ) {
		TRACE( RK628D_ERROR, "%s: pRegDesc is NULL!\n", __FUNCTION__ );
		return ( RET_NULL_POINTER );
	}

	/* write default values derived from datasheet and evaluation kit (static setup altered by dynamic setup further below) */
	result = IsiRegDefaultsApply( pRK628DCtx, pRegDesc );
	if ( result != RET_SUCCESS )
	{
		return ( result );
	}

	/* sleep a while, that sensor can take over new default values */
	osSleep( 10 );

	/* verify default values to make sure everything has been written correctly as expected */
	result = IsiRegDefaultsVerify( pRK628DCtx, pRegDesc );
	if ( result != RET_SUCCESS )
	{
		return ( result );
	}

	return ( result );
}
static void update_input_resolution(IsiSensorHandle_t handle)
{
    char buf[32] = { 0 };
    char prop_str[32] = { 0 };
    unsigned int hact = 0;
    unsigned int vact = 0;
    unsigned int fps = 0;
    unsigned int audio_rate = 0;
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    read(pRK628DCtx->res_fd, buf, sizeof(buf));
    lseek(pRK628DCtx->res_fd, 0, SEEK_SET);

    if (!strstr(buf, "unsupported")) {
	    // buf eg: 1920x1080P60@44100
	    if (sscanf(buf, "%dx%dP%d@%d", &hact, &vact, &fps, &audio_rate) < 3) {
		TRACE( RK628D_ERROR, "%s: get resolution failed, buf:%s\n",
		    __FUNCTION__, buf);
		property_set("sys.hdmiin.display", "0");
		property_set("sys.hdmiin.resolution", "false");
		property_set("vendor.hdmiin.audiorate", "false");
		return;
	    }

	    TRACE( RK628D_ERROR, "%s hact: %d, vact: %d, fps:%d, audio_rate:%d\n",
		__FUNCTION__, hact, vact, fps, audio_rate);
	    sprintf(prop_str, "%dx%dP%d", hact, vact, fps);
	    property_set("sys.hdmiin.resolution", prop_str);
    	    MEMSET(prop_str, 0, sizeof(prop_str));
	    sprintf(prop_str, "%d", audio_rate);
	    property_set("vendor.hdmiin.audiorate", prop_str);
    } else {
        TRACE( RK628D_ERROR, "%s unsupported resolution!!!", __func__);
	property_set("sys.hdmiin.display", "0");
    	property_set("sys.hdmiin.resolution", "false");
	property_set("vendor.hdmiin.audiorate", "false");
    }
}

static int32_t HdmiinThreadHandler
(
    void *p_arg
)
{
    IsiSensorHandle_t   handle = (IsiSensorHandle_t)p_arg;
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)p_arg;

    while(!pRK628DCtx->bHdmiinExit){
		update_input_resolution(handle);
		TRACE( RK628D_DEBUG,  "%s: -----hdmiin--- thread--------\n",  __FUNCTION__ );
		osSleep( 300 );
    }
    return 0;
}


/*****************************************************************************/
/**
 *          RK628D_IsiCreateSensorIss
 *
 * @brief   This function creates a new RK628D sensor instance handle.
 *
 * @param   pConfig     configuration structure to create the instance
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 * @retval  RET_OUTOFMEM
 *
 *****************************************************************************/
static RESULT RK628D_IsiCreateSensorIss
(
    IsiSensorInstanceConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;

    RK628D_Context_t *pRK628DCtx;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( (pConfig == NULL) || (pConfig->pSensor ==NULL) )
    {
        return ( RET_NULL_POINTER );
    }

    pRK628DCtx = ( RK628D_Context_t * )malloc ( sizeof (RK628D_Context_t) );
    if ( pRK628DCtx == NULL )
    {
        TRACE( RK628D_ERROR,  "%s: Can't allocate ov14825 context\n",  __FUNCTION__ );
        return ( RET_OUTOFMEM );
    }
    MEMSET( pRK628DCtx, 0, sizeof( RK628D_Context_t ) );

    result = HalAddRef( pConfig->HalHandle );
    if ( result != RET_SUCCESS )
    {
        free ( pRK628DCtx );
        return ( result );
    }

    pRK628DCtx->res_fd = open("/sys/class/rk628csi/resolution", O_RDONLY);
    if (pRK628DCtx->res_fd < 0) {
        TRACE( RK628D_ERROR, "Open /sys/class/rk628csi/resolution failed!" );
        return pRK628DCtx->res_fd;
    }

    pRK628DCtx->stream_fd = open("/sys/class/rk628csi/streamen", O_RDWR);
    if (pRK628DCtx->stream_fd < 0) {
        TRACE( RK628D_ERROR, "Open /sys/class/rk628csi/streamen failed!" );
        return pRK628DCtx->stream_fd;
    }

    pRK628DCtx->IsiCtx.HalHandle              = pConfig->HalHandle;
    pRK628DCtx->IsiCtx.HalDevID               = pConfig->HalDevID;
    pRK628DCtx->IsiCtx.I2cBusNum              = pConfig->I2cBusNum;
    pRK628DCtx->IsiCtx.SlaveAddress           = ( pConfig->SlaveAddr == 0 ) ? RK628D_SLAVE_ADDR : pConfig->SlaveAddr;
    pRK628DCtx->IsiCtx.NrOfAddressBytes       = 1U;

    pRK628DCtx->IsiCtx.I2cAfBusNum            = pConfig->I2cAfBusNum;
    pRK628DCtx->IsiCtx.SlaveAfAddress         = ( pConfig->SlaveAfAddr == 0 ) ? RK628D_SLAVE_AF_ADDR : pConfig->SlaveAfAddr;
    pRK628DCtx->IsiCtx.NrOfAfAddressBytes     = 0;

    pRK628DCtx->IsiCtx.pSensor                = pConfig->pSensor;

    pRK628DCtx->Configured             = BOOL_FALSE;
    pRK628DCtx->Streaming              = BOOL_FALSE;
    pRK628DCtx->TestPattern            = BOOL_FALSE;
    pRK628DCtx->isAfpsRun              = BOOL_FALSE;

    pRK628DCtx->IsiSensorMipiInfo.sensorHalDevID = pRK628DCtx->IsiCtx.HalDevID;
    if(pConfig->mipiLaneNum & g_suppoted_mipi_lanenum_type){
        pRK628DCtx->IsiSensorMipiInfo.ucMipiLanes = pConfig->mipiLaneNum;
		TRACE( RK628D_INFO, "%s set lane numbers :%d\n", __FUNCTION__,pConfig->mipiLaneNum);
    }else{
        TRACE( RK628D_ERROR, "%s don't support lane numbers :%d,set to default %d\n", __FUNCTION__,pConfig->mipiLaneNum,DEFAULT_NUM_LANES);
        pRK628DCtx->IsiSensorMipiInfo.ucMipiLanes = DEFAULT_NUM_LANES;
    }

    pConfig->hSensor = ( IsiSensorHandle_t )pRK628DCtx;
    pRK628DCtx->bHdmiinExit = false;
    pRK628DCtx->gStatus = STATUS_POWER_ON;
    osThreadCreate( &pRK628DCtx->gHdmiinThreadId, HdmiinThreadHandler, (void *)pRK628DCtx);

    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          RK628D_IsiReleaseSensorIss
 *
 * @brief   This function destroys/releases an RK628D sensor instance.
 *
 * @param   handle      RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 *
 *****************************************************************************/
static RESULT RK628D_IsiReleaseSensorIss
(
    IsiSensorHandle_t handle
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

//    (void)RK628D_IsiSensorSetStreamingIss( pLT6911CCtx, BOOL_FALSE );

    (void)HalDelRef( pRK628DCtx->IsiCtx.HalHandle );

    pRK628DCtx->bHdmiinExit = true;
	if ( OSLAYER_OK != osThreadWait( &pRK628DCtx->gHdmiinThreadId) )
		TRACE( RK628D_DEBUG, "%s wait hdmiiin listener thread exit\n", __FUNCTION__);
	if ( OSLAYER_OK != osThreadClose( &pRK628DCtx->gHdmiinThreadId ) )
		TRACE( RK628D_DEBUG, "%s hdmiiin listener thread exit\n", __FUNCTION__);

    pRK628DCtx->gStatus = STATUS_POWER_ON;
    property_set("sys.hdmiin.resolution", "false");
    close(pRK628DCtx->res_fd);
    close(pRK628DCtx->stream_fd);

    MEMSET( pRK628DCtx, 0, sizeof( RK628D_Context_t ) );
    free ( pRK628DCtx );
    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCapsIss
 *
 * @brief   fills in the correct pointers for the sensor description struct
 *
 * @param   param1      pointer to sensor capabilities structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCapsIssInternal
(
    IsiSensorCaps_t   *pIsiSensorCaps,
    uint32_t  mipi_lanes
)
{

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);


    if ( pIsiSensorCaps == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {

        if(mipi_lanes == SUPPORT_MIPI_FOUR_LANE){
            switch (pIsiSensorCaps->Index)
            {
                case 0:
                {
                    pIsiSensorCaps->Resolution = ISI_RES_3840_2160P30;
                    break;
                }
                case 1:
                {
                    pIsiSensorCaps->Resolution = ISI_RES_TV1080P60;
                    break;
                }
                case 2:
                {
                    pIsiSensorCaps->Resolution = ISI_RES_TV720P60;
                    break;
                }
                case 3:
                {
                    pIsiSensorCaps->Resolution = ISI_RES_720_576P50;
                    break;
                }
                case 4:
                {
                    pIsiSensorCaps->Resolution = ISI_RES_720_480P60;
                    break;
                }

                default:
                {
                    result = RET_OUTOFRANGE;
                    goto end;
                }
            }
        }

        pIsiSensorCaps->BusWidth        = ISI_BUSWIDTH_8BIT_ZZ;
        pIsiSensorCaps->Mode            = ISI_MODE_BT601;
        pIsiSensorCaps->FieldSelection  = ISI_FIELDSEL_BOTH;
        pIsiSensorCaps->YCSequence      = ISI_YCSEQ_CBYCRY;
        pIsiSensorCaps->Conv422         = ISI_CONV422_NOCOSITED;
        pIsiSensorCaps->BPat            = ISI_BPAT_RGRGGBGB ;
        pIsiSensorCaps->HPol            = ISI_HPOL_REFPOS; //hsync?
        pIsiSensorCaps->VPol            = ISI_VPOL_POS; //VPolarity
        pIsiSensorCaps->Edge            = ISI_EDGE_RISING;
        pIsiSensorCaps->Bls             = ISI_BLS_OFF; //close;
        pIsiSensorCaps->Gamma           = ISI_GAMMA_ON;
        pIsiSensorCaps->CConv           = ISI_CCONV_ON;
        pIsiSensorCaps->BLC             = ( ISI_BLC_AUTO );
        pIsiSensorCaps->AGC             = ( ISI_AGC_AUTO );
        pIsiSensorCaps->AWB             = ( ISI_AWB_AUTO );
        pIsiSensorCaps->AEC             = ( ISI_AEC_AUTO );
        pIsiSensorCaps->DPCC            = ( ISI_DPCC_AUTO );

        pIsiSensorCaps->DwnSz           = ISI_DWNSZ_SUBSMPL;
        pIsiSensorCaps->CieProfile      = 0;
        pIsiSensorCaps->SmiaMode        = ISI_SMIA_OFF;
        pIsiSensorCaps->MipiMode        = ISI_MIPI_MODE_YUV422_8;
        pIsiSensorCaps->AfpsResolutions = ( ISI_AFPS_NOTSUPP );
		pIsiSensorCaps->SensorOutputMode = ISI_SENSOR_OUTPUT_MODE_YUV;
    }
end:
    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

static RESULT RK628D_IsiGetCapsIss
(
    IsiSensorHandle_t handle,
    IsiSensorCaps_t   *pIsiSensorCaps
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    result = RK628D_IsiGetCapsIssInternal(pIsiSensorCaps, pRK628DCtx->IsiSensorMipiInfo.ucMipiLanes);



	TRACE( RK628D_DEBUG, "%d ( pIsiSensorCaps->Index)\n", pIsiSensorCaps->Index);
	TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_g_IsiSensorDefaultConfig
 *
 * @brief   recommended default configuration for application use via call
 *          to IsiGetSensorIss()
 *
 *****************************************************************************/
const IsiSensorCaps_t RK628D_g_IsiSensorDefaultConfig =
{
    ISI_BUSWIDTH_8BIT_ZZ,         // BusWidth
    ISI_MODE_BT601, //ISI_MODE_MIPI,              // MIPI
    ISI_FIELDSEL_BOTH,          // FieldSel
    ISI_YCSEQ_CBYCRY,           // YCSeq
    ISI_CONV422_NOCOSITED,      // Conv422
    ISI_BPAT_BGBGGRGR,//ISI_BPAT_BGBGGRGR,          // BPat
    ISI_HPOL_REFPOS,            // HPol
    ISI_VPOL_POS,               // VPol
    ISI_EDGE_FALLING,            // Edge
    ISI_BLS_OFF,                // Bls
    ISI_GAMMA_OFF,              // Gamma
    ISI_CCONV_OFF,              // CConv
    ISI_RES_SVGAP30,          // Res
    ISI_DWNSZ_SUBSMPL,          // DwnSz
    ISI_BLC_OFF ,               // BLC
    ISI_AGC_OFF,                // AGC
    ISI_AWB_OFF,                // AWB
    ISI_AEC_OFF,                // AEC
    ISI_DPCC_OFF,               // DPCC
    0,            // CieProfile, this is also used as start profile for AWB (if not altered by menu settings)
    ISI_SMIA_OFF,               // SmiaMode
    ISI_MIPI_MODE_YUV422_8,       // MipiMode
    ISI_AFPS_NOTSUPP,           // AfpsResolutions
    ISI_SENSOR_OUTPUT_MODE_YUV,
    0,
};



/*****************************************************************************/
/**
 *          RK628D_SetupOutputFormat
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      RK628D sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_SetupOutputFormat
(
    RK628D_Context_t       *pRK628DCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;
    TRACE( RK628D_INFO, "%s%s (enter)\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );

    /* bus-width */
    switch ( pConfig->BusWidth )        /* only ISI_BUSWIDTH_12BIT supported, no configuration needed here */
    {
        case ISI_BUSWIDTH_8BIT_ZZ:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: bus width not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* mode */
    switch ( pConfig->Mode )            /* only ISI_MODE_BAYER supported, no configuration needed here */
    {
        case( ISI_MODE_MIPI ):
        case ISI_MODE_BAYER:
        case ISI_MODE_BT601:
        case ISI_MODE_PICT:
        case ISI_MODE_DATA:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: mode not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* field-selection */
    switch ( pConfig->FieldSelection )  /* only ISI_FIELDSEL_BOTH supported, no configuration needed */
    {
        case ISI_FIELDSEL_BOTH:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: field selection not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* only Bayer mode is supported by RK628D sensor, so the YCSequence parameter is not checked */
    switch ( pConfig->YCSequence )
    {
        default:
        {
            break;
        }
    }

    /* 422 conversion */
    switch ( pConfig->Conv422 )         /* only ISI_CONV422_NOCOSITED supported, no configuration needed */
    {
        case ISI_CONV422_NOCOSITED:
        case ISI_CONV422_COSITED:
        case ISI_CONV422_INTER:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: 422 conversion not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* bayer-pattern */
    switch ( pConfig->BPat )            /* only ISI_BPAT_BGBGGRGR supported, no configuration needed */
    {
        case ISI_BPAT_BGBGGRGR:
        case ISI_BPAT_RGRGGBGB:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: bayer pattern not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* horizontal polarity */
    switch ( pConfig->HPol )            /* only ISI_HPOL_REFPOS supported, no configuration needed */
    {
        case ISI_HPOL_REFPOS:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: HPol not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* vertical polarity */
    switch ( pConfig->VPol )            /*no configuration needed */
    {
        case ISI_VPOL_NEG:
        {
            break;
        }
        case ISI_VPOL_POS:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: VPol not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }


    /* edge */
    switch ( pConfig->Edge )            /* only ISI_EDGE_RISING supported, no configuration needed */
    {
        case ISI_EDGE_RISING:
        {
            break;
        }

        case ISI_EDGE_FALLING:          /*TODO for MIPI debug*/
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s:  edge mode not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* gamma */
    switch ( pConfig->Gamma )           /* only ISI_GAMMA_OFF supported, no configuration needed */
    {
        case ISI_GAMMA_ON:
        case ISI_GAMMA_OFF:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s:  gamma not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    /* color conversion */
    switch ( pConfig->CConv )           /* only ISI_CCONV_OFF supported, no configuration needed */
    {
        case ISI_CCONV_OFF:
        case ISI_CCONV_ON:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: color conversion not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->SmiaMode )        /* only ISI_SMIA_OFF supported, no configuration needed */
    {
        case ISI_SMIA_OFF:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: SMIA mode not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->MipiMode )        /* only ISI_MIPI_MODE_RAW_12 supported, no configuration needed */
    {
        case ISI_MIPI_MODE_RAW_10:
		case ISI_MIPI_MODE_YUV422_8:
        case ISI_MIPI_OFF:
        {
            break;
        }

        default:
        {
            TRACE( RK628D_ERROR, "%s%s: MIPI mode not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            return ( RET_NOTSUPP );
        }
    }

    switch ( pConfig->AfpsResolutions ) /* no configuration needed */
    {
        case ISI_AFPS_NOTSUPP:
        {
            break;
        }
        default:
        {
            // don't care about what comes in here
            //TRACE(RK628DC_ERROR, "%s%s: AFPS not supported\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"" );
            //return ( RET_NOTSUPP );
        }
    }

    TRACE( RK628D_INFO, "%s%s (exit)\n", __FUNCTION__, pRK628DCtx->isAfpsRun?"(AFPS)":"");
	return ( result );

}

int RK628D_get_PCLK( RK628D_Context_t *pRK628DCtx, int XVCLK)
{
    // calculate PCLK

    return 0;
}

/*****************************************************************************/
/**
 *          RK628D_SetupOutputWindow
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      RK628D sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_SetupOutputWindow
(
    RK628D_Context_t        *pRK628DCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result     = RET_SUCCESS;

	TRACE( RK628D_INFO, "%s (enter.....)\n", __FUNCTION__);

    switch ( pConfig->Resolution )
    {
        default:
        {
            pRK628DCtx->IsiSensorMipiInfo.ulMipiFreq = 800;
            break;
        }
    }

	TRACE( RK628D_ERROR, "%s:%d ulMipiFreq:%d\n", __FUNCTION__, __LINE__,
			pRK628DCtx->IsiSensorMipiInfo.ulMipiFreq);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_SetupImageControl
 *
 * @brief   Sets the image control functions (BLC, AGC, AWB, AEC, DPCC ...)
 *
 * @param   handle      RK628D sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_SetupImageControl
(
    RK628D_Context_t        *pRK628DCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;
    uint32_t RegValue = 0U;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_AecSetModeParameters
 *
 * @brief   This function fills in the correct parameters in RK628D-Instances
 *          according to AEC mode selection in IsiSensorConfig_t.
 *
 * @note    It is assumed that IsiSetupOutputWindow has been called before
 *          to fill in correct values in instance structure.
 *
 * @param   handle      RK628D context
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_AecSetModeParameters
(
    RK628D_Context_t       *pRK628DCtx,
    const IsiSensorConfig_t *pConfig
)
{
    RESULT result = RET_SUCCESS;

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiSetupSensorIss
 *
 * @brief   Setup of the image sensor considering the given configuration.
 *
 * @param   handle      RK628D sensor instance handle
 * @param   pConfig     pointer to sensor configuration structure
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiSetupSensorIss
(
    IsiSensorHandle_t       handle,
    const IsiSensorConfig_t *pConfig
)
{
    uint32_t data;
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    uint32_t RegValue = 0;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        TRACE( RK628D_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pConfig == NULL )
    {
        TRACE( RK628D_ERROR, "%s: Invalid configuration (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_NULL_POINTER );
    }

    if ( pRK628DCtx->Streaming != BOOL_FALSE )
    {
        return RET_WRONG_STATE;
    }

    MEMCPY( &pRK628DCtx->Config, pConfig, sizeof( IsiSensorConfig_t ) );

	    /* 4.) setup output format */
	    result = RK628D_SetupOutputFormat( pRK628DCtx, pConfig );
	    if ( result != RET_SUCCESS )
	    {
	        TRACE( RK628D_ERROR, "%s: SetupOutputFormat failed.\n", __FUNCTION__);
	        return ( result );
	    }

	    /* 5.) setup output window */
	    result = RK628D_SetupOutputWindow( pRK628DCtx, pConfig );
	    if ( result != RET_SUCCESS )
	    {
	        TRACE( RK628D_ERROR, "%s: SetupOutputWindow failed.\n", __FUNCTION__);
	        return ( result );
	    }

	    result = RK628D_SetupImageControl( pRK628DCtx, pConfig );
	    if ( result != RET_SUCCESS )
	    {
	        TRACE( RK628D_ERROR, "%s: SetupImageControl failed.\n", __FUNCTION__);
	        return ( result );
	    }

	    result = RK628D_AecSetModeParameters( pRK628DCtx, pConfig );
	    if ( result != RET_SUCCESS )
	    {
	        TRACE( RK628D_ERROR, "%s: AecSetModeParameters failed.\n", __FUNCTION__);
	        return ( result );
	    }

    if (result == RET_SUCCESS)
    {
        pRK628DCtx->Configured = BOOL_TRUE;
    }

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiChangeSensorResolutionIss
 *
 * @brief   Change image sensor resolution while keeping all other static settings.
 *          Dynamic settings like current gain & integration time are kept as
 *          close as possible. Sensor needs 2 frames to engage (first 2 frames
 *          are not correctly exposed!).
 *
 * @note    Re-read current & min/max values as they will probably have changed!
 *
 * @param   handle                  Sensor instance handle
 * @param   Resolution              new resolution ID
 * @param   pNumberOfFramesToSkip   reference to storage for number of frames to skip
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_WRONG_STATE
 * @retval  RET_OUTOFRANGE
 *
 *****************************************************************************/
static RESULT RK628D_IsiChangeSensorResolutionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            Resolution,
    uint8_t             *pNumberOfFramesToSkip
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if (pNumberOfFramesToSkip == NULL)
    {
        return ( RET_NULL_POINTER );
    }

    if ( (pRK628DCtx->Configured != BOOL_TRUE) || (pRK628DCtx->Streaming != BOOL_FALSE) )
    {
        return RET_WRONG_STATE;
    }

    IsiSensorCaps_t Caps;
    Caps.Index = 0;
    Caps.Resolution = 0;
    while (RK628D_IsiGetCapsIss( handle, &Caps) == RET_SUCCESS) {
        if (Resolution == Caps.Resolution) {
            break;
        }
        Caps.Index++;
    }

    if ( (Resolution & Caps.Resolution) == 0 )
    {
        return RET_OUTOFRANGE;
    }

    if ( Resolution == pRK628DCtx->Config.Resolution )
    {
        // well, no need to worry
        *pNumberOfFramesToSkip = 0;
    }
    else
    {
        // change resolution
        char *szResName = NULL;
        result = IsiGetResolutionName( Resolution, &szResName );
        TRACE( RK628D_DEBUG, "%s: NewRes=0x%08x (%s)\n", __FUNCTION__, Resolution, szResName);

        // update resolution in copy of config in context
        pRK628DCtx->Config.Resolution = Resolution;

        // tell sensor about that
        result = RK628D_SetupOutputWindow( pRK628DCtx, &pRK628DCtx->Config );
        if ( result != RET_SUCCESS )
        {
            TRACE( RK628D_ERROR, "%s: SetupOutputWindow failed.\n", __FUNCTION__);
            return ( result );
        }

        // remember old exposure values
        float OldGain = pRK628DCtx->AecCurGain;
        float OldIntegrationTime = pRK628DCtx->AecCurIntegrationTime;

        // update limits & stuff (reset current & old settings)
        result = RK628D_AecSetModeParameters( pRK628DCtx, &pRK628DCtx->Config );
        if ( result != RET_SUCCESS )
        {
            TRACE( RK628D_ERROR, "%s: AecSetModeParameters failed.\n", __FUNCTION__);
            return ( result );
        }

        // restore old exposure values (at least within new exposure values' limits)
        uint8_t NumberOfFramesToSkip = 2;
        float   DummySetGain;
        float   DummySetIntegrationTime;
        result = RK628D_IsiExposureControlIss( handle, OldGain, OldIntegrationTime, &NumberOfFramesToSkip, &DummySetGain, &DummySetIntegrationTime );
        if ( result != RET_SUCCESS )
        {
            TRACE( RK628D_ERROR, "%s: RK628D_IsiExposureControlIss failed.\n", __FUNCTION__);
            return ( result );
        }

        // return number of frames that aren't exposed correctly
        *pNumberOfFramesToSkip = NumberOfFramesToSkip + 1;
    }

    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiSensorSetStreamingIss
 *
 * @brief   Enables/disables streaming of sensor data, if possible.
 *
 * @param   handle      Sensor instance handle
 * @param   on          new streaming state (BOOL_TRUE=on, BOOL_FALSE=off)
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_WRONG_STATE
 *
 *****************************************************************************/
static RESULT RK628D_IsiSensorSetStreamingIss
(
    IsiSensorHandle_t   handle,
    bool_t              on
)
{
    uint32_t RegValue = 0;
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;
    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);
    int ret;
    char buf[32] = { 0 };

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( (pRK628DCtx->Configured != BOOL_TRUE) || (pRK628DCtx->Streaming == on) )
    {
        return RET_WRONG_STATE;
    }

    if (on == BOOL_TRUE)
    {
        /* enable streaming */
        lseek(pRK628DCtx->stream_fd, 0, SEEK_SET);
        ret = read(pRK628DCtx->stream_fd, buf, sizeof(buf));
        TRACE( RK628D_ERROR, "%s ret:%d, read buf:%s\n", __FUNCTION__, ret, buf);
        lseek(pRK628DCtx->stream_fd, 0, SEEK_SET);
        ret = write(pRK628DCtx->stream_fd, "1", sizeof("1"));
        TRACE( RK628D_ERROR, "%s stream on, write ret:%d\n", __FUNCTION__, ret);
        result = RET_SUCCESS;
    }
    else
    {
        /* disable streaming */
        lseek(pRK628DCtx->stream_fd, 0, SEEK_SET);
        ret = read(pRK628DCtx->stream_fd, buf, sizeof(buf));
        TRACE( RK628D_ERROR, "%s ret:%d, read buf:%s\n", __FUNCTION__, ret, buf);
        lseek(pRK628DCtx->stream_fd, 0, SEEK_SET);
        ret = write(pRK628DCtx->stream_fd, "0", sizeof("0"));
        TRACE( RK628D_ERROR, "%s stream off, write ret:%d\n", __FUNCTION__, ret);
        result = RET_SUCCESS;
    }

    if (result == RET_SUCCESS)
    {
        pRK628DCtx->Streaming = on;
    }

    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiSensorSetPowerIss
 *
 * @brief   Performs the power-up/power-down sequence of the camera, if possible.
 *
 * @param   handle      RK628D sensor instance handle
 * @param   on          new power state (BOOL_TRUE=on, BOOL_FALSE=off)
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiSensorSetPowerIss
(
    IsiSensorHandle_t   handle,
    bool_t              on
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    pRK628DCtx->Configured = BOOL_FALSE;
    pRK628DCtx->Streaming  = BOOL_FALSE;
    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiCheckSensorConnectionIss
 *
 * @brief   Checks the I2C-Connection to sensor by reading sensor revision id.
 *
 * @param   handle      RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiCheckSensorConnectionIss
(
    IsiSensorHandle_t   handle
)
{
    uint32_t RevId;
    uint32_t value;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }
	result = RK628D_IsiGetSensorRevisionIss( handle, &value );
#if 0
    RevId = RK628D_CHIP_ID_HIGH_BYTE_DEFAULT;
    //RevId = (RevId << 16U) | (RK628D_CHIP_ID_MIDDLE_BYTE_DEFAULT<<8U);
    //RevId = RevId | RK628D_CHIP_ID_LOW_BYTE_DEFAULT;

    if ( (result != RET_SUCCESS) || (RevId != value) )
    {
        TRACE( RK628D_ERROR, "%s RevId = 0x%08x, value = 0x%08x \n", __FUNCTION__, RevId, value );
        return ( RET_FAILURE );
    }

    TRACE( RK628D_DEBUG, "%s RevId = 0x%08x, value = 0x%08x \n", __FUNCTION__, RevId, value );
#endif
    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetSensorRevisionIss
 *
 * @brief   reads the sensor revision register and returns this value
 *
 * @param   handle      pointer to sensor description struct
 * @param   p_value     pointer to storage value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetSensorRevisionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            *p_value
)
{
    RESULT result = RET_SUCCESS;

    uint32_t data;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);
#if 0
    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( p_value == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *p_value = 0U;
    result = RK628D_IsiRegReadIss ( handle, RK628D_CHIP_ID_HIGH_BYTE, &data );
	/*
    *p_value = ( (data & 0xFF) << 16U );
    result = RK628D_IsiRegReadIss ( handle, RK628D_CHIP_ID_MIDDLE_BYTE, &data );
    *p_value |= ( (data & 0xFF) << 8U );
    result = RK628D_IsiRegReadIss ( handle, RK628D_CHIP_ID_LOW_BYTE, &data );
    *p_value |= ( (data & 0xFF));
    */
    *p_value = data;

    TRACE( RK628D_DEBUG, "%s (exit),*p_value=0x%08x,data=0x%08x\n", __FUNCTION__,*p_value,data);
#endif

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiRegReadIss
 *
 * @brief   grants user read access to the camera register
 *
 * @param   handle      pointer to sensor description struct
 * @param   address     sensor register to write
 * @param   p_value     pointer to value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiRegReadIss
(
    IsiSensorHandle_t   handle,
    const uint32_t      address,
    uint32_t            *p_value
)
{
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( p_value == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else
    {
        uint8_t NrOfBytes = GetNrDatBytesOfAllRegTable(address);
        TRACE( RK628D_DEBUG, "%s (IsiGetNrDatBytesIss %d 0x%08x)\n", __FUNCTION__, NrOfBytes, address);
        if ( !NrOfBytes )
        {
            NrOfBytes = 1;
        }

        *p_value = 0;
        result = IsiI2cReadSensorRegister( handle, address, (uint8_t *)p_value, NrOfBytes, BOOL_TRUE );
    }

    TRACE( RK628D_DEBUG, "%s (exit: 0x%08x 0x%08x)\n", __FUNCTION__, address, *p_value);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiRegWriteIss
 *
 * @brief   grants user write access to the camera register
 *
 * @param   handle      pointer to sensor description struct
 * @param   address     sensor register to write
 * @param   value       value to write
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 *
 *****************************************************************************/
static RESULT RK628D_IsiRegWriteIss
(
    IsiSensorHandle_t   handle,
    const uint32_t      address,
    const uint32_t      value
)
{
    RESULT result = RET_SUCCESS;

    uint8_t NrOfBytes;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( handle == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    NrOfBytes = GetNrDatBytesOfAllRegTable(address);
    TRACE( RK628D_DEBUG, "%s (IsiGetNrDatBytesIss %d 0x%08x 0x%08x)\n", __FUNCTION__, NrOfBytes, address, value);
    if ( !NrOfBytes )
    {
        NrOfBytes = 1;
    }

    result = IsiI2cWriteSensorRegister( handle, address, (uint8_t *)(&value), NrOfBytes, BOOL_TRUE );

    TRACE( RK628D_DEBUG, "%s (exit: 0x%08x 0x%08x)\n", __FUNCTION__, address, value);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetGainLimitsIss
 *
 * @brief   Returns the exposure minimal and maximal values of an
 *          RK628D instance
 *
 * @param   handle       RK628D sensor instance handle
 * @param   pMinExposure Pointer to a variable receiving minimal exposure value
 * @param   pMaxExposure Pointer to a variable receiving maximal exposure value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetGainLimitsIss
(
    IsiSensorHandle_t   handle,
    float               *pMinGain,
    float               *pMaxGain
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    uint32_t RegValue = 0;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    *pMinGain = 0;
    *pMaxGain = 0;

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetIntegrationTimeLimitsIss
 *
 * @brief   Returns the minimal and maximal integration time values of an
 *          RK628D instance
 *
 * @param   handle       RK628D sensor instance handle
 * @param   pMinExposure Pointer to a variable receiving minimal exposure value
 * @param   pMaxExposure Pointer to a variable receiving maximal exposure value
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetIntegrationTimeLimitsIss
(
    IsiSensorHandle_t   handle,
    float               *pMinIntegrationTime,
    float               *pMaxIntegrationTime
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;
    uint32_t RegValue = 0;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetGainIss
 *
 * @brief   Reads gain values from the image sensor module.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   pSetGain                set gain
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetGainIss
(
    IsiSensorHandle_t   handle,
    float               *pSetGain
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    *pSetGain = 0;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetGainIncrementIss
 *
 * @brief   Get smallest possible gain increment.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   pIncr                   increment
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetGainIncrementIss
(
    IsiSensorHandle_t   handle,
    float               *pIncr
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    *pIncr = 0;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiSetGainIss
 *
 * @brief   Writes gain values to the image sensor module.
 *          Updates current gain and exposure in sensor struct/state.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   NewGain                 gain to be set
 * @param   pSetGain                set gain
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 *
 *****************************************************************************/
RESULT RK628D_IsiSetGainIss
(
    IsiSensorHandle_t   handle,
    float               NewGain,
    float               *pSetGain
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;
    uint16_t usGain = 0;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetIntegrationTimeIss
 *
 * @brief   Reads integration time values from the image sensor module.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   pSetIntegrationTime     set integration time
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetIntegrationTimeIss
(
    IsiSensorHandle_t   handle,
    float               *pSetIntegrationTime
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetIntegrationTimeIncrementIss
 *
 * @brief   Get smallest possible integration time increment.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   pIncr                   increment
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetIntegrationTimeIncrementIss
(
    IsiSensorHandle_t   handle,
    float               *pIncr
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiSetIntegrationTimeIss
 *
 * @brief   Writes gain and integration time values to the image sensor module.
 *          Updates current integration time and exposure in sensor
 *          struct/state.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   NewIntegrationTime      integration time to be set
 * @param   pSetIntegrationTime     set integration time
 * @param   pNumberOfFramesToSkip   number of frames to skip until AE is
 *                                  executed again
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 * @retval  RET_DIVISION_BY_ZERO
 *
 *****************************************************************************/
RESULT RK628D_IsiSetIntegrationTimeIss
(
    IsiSensorHandle_t   handle,
    float               NewIntegrationTime,
    float               *pSetIntegrationTime,
    uint8_t             *pNumberOfFramesToSkip
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_DEBUG, "%s: Ti=%f\n", __FUNCTION__, *pSetIntegrationTime );
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiExposureControlIss
 *
 * @brief   Camera hardware dependent part of the exposure control loop.
 *          Calculates appropriate register settings from the new exposure
 *          values and writes them to the image sensor module.
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   NewGain                 newly calculated gain to be set
 * @param   NewIntegrationTime      newly calculated integration time to be set
 * @param   pNumberOfFramesToSkip   number of frames to skip until AE is
 *                                  executed again
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_INVALID_PARM
 * @retval  RET_FAILURE
 * @retval  RET_DIVISION_BY_ZERO
 *
 *****************************************************************************/
RESULT RK628D_IsiExposureControlIss
(
    IsiSensorHandle_t   handle,
    float               NewGain,
    float               NewIntegrationTime,
    uint8_t             *pNumberOfFramesToSkip,
    float               *pSetGain,
    float               *pSetIntegrationTime
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);
    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCurrentExposureIss
 *
 * @brief   Returns the currently adjusted AE values
 *
 * @param   handle                  RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetCurrentExposureIss
(
    IsiSensorHandle_t   handle,
    float               *pSetGain,
    float               *pSetIntegrationTime
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;
    uint32_t RegValue = 0;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetResolutionIss
 *
 * @brief   Reads integration time values from the image sensor module.
 *
 * @param   handle                  sensor instance handle
 * @param   pSettResolution         set resolution
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetResolutionIss
(
    IsiSensorHandle_t   handle,
    uint32_t            *pSetResolution
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;
    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        TRACE( RK628D_ERROR, "%s: Invalid sensor handle (NULL pointer detected)\n", __FUNCTION__ );
        return ( RET_WRONG_HANDLE );
    }

    if ( pSetResolution == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pSetResolution = pRK628DCtx->Config.Resolution;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetAfpsInfoHelperIss
 *
 * @brief   Calc AFPS sub resolution settings for the given resolution
 *
 * @param   pRK628DCtx             RK628D sensor instance (dummy!) context
 * @param   Resolution              Any supported resolution to query AFPS params for
 * @param   pAfpsInfo               Reference of AFPS info structure to write the results to
 * @param   AfpsStageIdx            Index of current AFPS stage to use
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetAfpsInfoHelperIss(
    RK628D_Context_t   *pRK628DCtx,
    uint32_t            Resolution,
    IsiAfpsInfo_t*      pAfpsInfo,
    uint32_t            AfpsStageIdx
)
{
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetAfpsInfoIss
 *
 * @brief   Returns the possible AFPS sub resolution settings for the given resolution series
 *
 * @param   handle                  RK628D sensor instance handle
 * @param   Resolution              Any resolution within the AFPS group to query;
 *                                  0 (zero) to use the currently configured resolution
 * @param   pAfpsInfo               Reference of AFPS info structure to store the results
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 * @retval  RET_NOTSUPP
 *
 *****************************************************************************/
RESULT RK628D_IsiGetAfpsInfoIss(
    IsiSensorHandle_t   handle,
    uint32_t            Resolution,
    IsiAfpsInfo_t*      pAfpsInfo
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibKFactor
 *
 * @brief   Returns the RK628D specific K-Factor
 *
 * @param   handle       RK628D sensor instance handle
 * @param   pIsiKFactor  Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibKFactor
(
    IsiSensorHandle_t   handle,
    Isi1x1FloatMatrix_t **pIsiKFactor
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiKFactor == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pIsiKFactor = NULL;

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibPcaMatrix
 *
 * @brief   Returns the RK628D specific PCA-Matrix
 *
 * @param   handle          RK628D sensor instance handle
 * @param   pIsiPcaMatrix   Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibPcaMatrix
(
    IsiSensorHandle_t   handle,
    Isi3x2FloatMatrix_t **pIsiPcaMatrix
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiPcaMatrix == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pIsiPcaMatrix = NULL;

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibSvdMeanValue
 *
 * @brief   Returns the sensor specific SvdMean-Vector
 *
 * @param   handle              RK628D sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibSvdMeanValue
(
    IsiSensorHandle_t   handle,
    Isi3x1FloatMatrix_t **pIsiSvdMeanValue
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiSvdMeanValue == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pIsiSvdMeanValue = NULL;

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibSvdMeanValue
 *
 * @brief   Returns a pointer to the sensor specific centerline, a straight
 *          line in Hesse normal form in Rg/Bg colorspace
 *
 * @param   handle              RK628D sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibCenterLine
(
    IsiSensorHandle_t   handle,
    IsiLine_t           **ptIsiCenterLine
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiCenterLine == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *ptIsiCenterLine = NULL;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibClipParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for Rg/Bg color
 *          space clipping
 *
 * @param   handle              RK628D sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibClipParam
(
    IsiSensorHandle_t   handle,
    IsiAwbClipParm_t    **pIsiClipParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pIsiClipParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *pIsiClipParam = NULL;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibGlobalFadeParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for AWB out of
 *          range handling
 *
 * @param   handle              RK628D sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibGlobalFadeParam
(
    IsiSensorHandle_t       handle,
    IsiAwbGlobalFadeParm_t  **ptIsiGlobalFadeParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);
    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiGlobalFadeParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *ptIsiGlobalFadeParam = NULL;
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetCalibFadeParam
 *
 * @brief   Returns a pointer to the sensor specific arrays for near white
 *          pixel parameter calculations
 *
 * @param   handle              RK628D sensor instance handle
 * @param   pIsiSvdMeanValue    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetCalibFadeParam
(
    IsiSensorHandle_t   handle,
    IsiAwbFade2Parm_t   **ptIsiFadeParam
)
{
    IsiSensorContext_t *pSensorCtx = (IsiSensorContext_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pSensorCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiFadeParam == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    *ptIsiFadeParam = NULL;

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetIlluProfile
 *
 * @brief   Returns a pointer to illumination profile idetified by CieProfile
 *          bitmask
 *
 * @param   handle              sensor instance handle
 * @param   CieProfile
 * @param   ptIsiIlluProfile    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetIlluProfile
(
    IsiSensorHandle_t   handle,
    const uint32_t      CieProfile,
    IsiIlluProfile_t    **ptIsiIlluProfile
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( ptIsiIlluProfile == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          RK628D_IsiGetLscMatrixTable
 *
 * @brief   Returns a pointer to illumination profile idetified by CieProfile
 *          bitmask
 *
 * @param   handle              sensor instance handle
 * @param   CieProfile
 * @param   ptIsiIlluProfile    Pointer to Pointer receiving the memory address
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiGetLscMatrixTable
(
    IsiSensorHandle_t   handle,
    const uint32_t      CieProfile,
    IsiLscMatrixTable_t **pLscMatrixTable
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pLscMatrixTable == NULL )
    {
        return ( RET_NULL_POINTER );
    }
    else

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}


/*****************************************************************************/
/**
 *          RK628D_IsiMdiInitMotoDriveMds
 *
 * @brief   General initialisation tasks like I/O initialisation.
 *
 * @param   handle              RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiMdiInitMotoDriveMds
(
    IsiSensorHandle_t   handle
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}



/*****************************************************************************/
/**
 *          RK628D_IsiMdiSetupMotoDrive
 *
 * @brief   Setup of the MotoDrive and return possible max step.
 *
 * @param   handle          RK628D sensor instance handle
 *          pMaxStep        pointer to variable to receive the maximum
 *                          possible focus step
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiMdiSetupMotoDrive
(
    IsiSensorHandle_t   handle,
    uint32_t            *pMaxStep
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pMaxStep == NULL )
    {
        return ( RET_NULL_POINTER );
    }


    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiMdiFocusSet
 *
 * @brief   Drives the lens system to a certain focus point.
 *
 * @param   handle          RK628D sensor instance handle
 *          AbsStep         absolute focus point to apply
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiMdiFocusSet
(
    IsiSensorHandle_t   handle,
    const uint32_t      Position
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    uint32_t nPosition;
    uint8_t  data[2] = { 0, 0 };

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }
    RETURN_RESULT_IF_DIFFERENT( RET_SUCCESS, result );

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiMdiFocusGet
 *
 * @brief   Retrieves the currently applied focus point.
 *
 * @param   handle          RK628D sensor instance handle
 *          pAbsStep        pointer to a variable to receive the current
 *                          focus point
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiMdiFocusGet
(
    IsiSensorHandle_t   handle,
    uint32_t            *pAbsStep
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;
    uint8_t  data[2] = { 0, 0 };

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    if ( pAbsStep == NULL )
    {
        return ( RET_NULL_POINTER );
    }

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiMdiFocusCalibrate
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
static RESULT RK628D_IsiMdiFocusCalibrate
(
    IsiSensorHandle_t   handle
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }

    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiActivateTestPattern
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 ******************************************************************************/
static RESULT RK628D_IsiActivateTestPattern
(
    IsiSensorHandle_t   handle,
    const bool_t        enable
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    return ( result );
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetSensorMipiInfoIss
 *
 * @brief   Triggers a forced calibration of the focus hardware.
 *
 * @param   handle          RK628D sensor instance handle
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_WRONG_HANDLE
 * @retval  RET_NULL_POINTER
 *
 ******************************************************************************/
static RESULT RK628D_IsiGetSensorMipiInfoIss
(
    IsiSensorHandle_t   handle,
    IsiSensorMipiInfo   *ptIsiSensorMipiInfo
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;

    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
        return ( RET_WRONG_HANDLE );
    }


    if ( ptIsiSensorMipiInfo == NULL )
    {
        return ( result );
    }

    ptIsiSensorMipiInfo->ucMipiLanes = pRK628DCtx->IsiSensorMipiInfo.ucMipiLanes;
    ptIsiSensorMipiInfo->ulMipiFreq= pRK628DCtx->IsiSensorMipiInfo.ulMipiFreq;
    ptIsiSensorMipiInfo->sensorHalDevID = pRK628DCtx->IsiSensorMipiInfo.sensorHalDevID;

    TRACE( RK628D_DEBUG, "ucMipiLanes=%d,ulMipiFreq=%d,sensorHalDevID=%d\n",  ptIsiSensorMipiInfo->ucMipiLanes,ptIsiSensorMipiInfo->ulMipiFreq,ptIsiSensorMipiInfo->sensorHalDevID );
    TRACE( RK628D_INFO, "%s: (exit)\n", __FUNCTION__);

    return ( result );
}

static RESULT RK628D_IsiGetSensorIsiVersion
(  IsiSensorHandle_t   handle,
   unsigned int*     pVersion
)
{
    RK628D_Context_t *pRK628DCtx = (RK628D_Context_t *)handle;
	uint8_t p_value;
	int ret;

    RESULT result = RET_SUCCESS;


    TRACE( RK628D_INFO, "%s: (enter)\n", __FUNCTION__);

    if ( pRK628DCtx == NULL )
    {
    	TRACE( RK628D_ERROR, "%s: pRK628DCtx IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
    }

	if(pVersion == NULL)
	{
		TRACE( RK628D_ERROR, "%s: pVersion IS NULL\n", __FUNCTION__);
        return ( RET_WRONG_HANDLE );
	}

	*pVersion = CONFIG_ISI_VERSION;

	return result;
}

/*****************************************************************************/
/**
 *          RK628D_IsiGetSensorIss
 *
 * @brief   fills in the correct pointers for the sensor description struct
 *
 * @param   param1      pointer to sensor description struct
 *
 * @return  Return the result of the function call.
 * @retval  RET_SUCCESS
 * @retval  RET_NULL_POINTER
 *
 *****************************************************************************/
RESULT RK628D_IsiGetSensorIss
(
    IsiSensor_t *pIsiSensor
)
{
    RESULT result = RET_SUCCESS;

    TRACE( RK628D_INFO, "%s (enter)\n", __FUNCTION__);

    if ( pIsiSensor != NULL )
    {
        pIsiSensor->pszName                             = RK628D_g_acName;
        pIsiSensor->pRegisterTable                      = RK628D_g_aRegDescription;
        pIsiSensor->pIsiSensorCaps                      = &RK628D_g_IsiSensorDefaultConfig;
	pIsiSensor->pIsiGetSensorIsiVer			= RK628D_IsiGetSensorIsiVersion;

        pIsiSensor->pIsiCreateSensorIss                 = RK628D_IsiCreateSensorIss;
        pIsiSensor->pIsiReleaseSensorIss                = RK628D_IsiReleaseSensorIss;
        pIsiSensor->pIsiGetCapsIss                      = RK628D_IsiGetCapsIss;
        pIsiSensor->pIsiSetupSensorIss                  = RK628D_IsiSetupSensorIss;
        pIsiSensor->pIsiChangeSensorResolutionIss       = RK628D_IsiChangeSensorResolutionIss;
        pIsiSensor->pIsiSensorSetStreamingIss           = RK628D_IsiSensorSetStreamingIss;
        pIsiSensor->pIsiSensorSetPowerIss               = RK628D_IsiSensorSetPowerIss;
        pIsiSensor->pIsiCheckSensorConnectionIss        = RK628D_IsiCheckSensorConnectionIss;
        pIsiSensor->pIsiGetSensorRevisionIss            = RK628D_IsiGetSensorRevisionIss;
        pIsiSensor->pIsiRegisterReadIss                 = RK628D_IsiRegReadIss;
        pIsiSensor->pIsiRegisterWriteIss                = RK628D_IsiRegWriteIss;
        pIsiSensor->pIsiGetResolutionIss                = RK628D_IsiGetResolutionIss;
        pIsiSensor->pIsiIsEvenFieldIss                  = NULL;

        /* AEC functions */
        pIsiSensor->pIsiExposureControlIss              = RK628D_IsiExposureControlIss;
        pIsiSensor->pIsiGetGainLimitsIss                = RK628D_IsiGetGainLimitsIss;
        pIsiSensor->pIsiGetIntegrationTimeLimitsIss     = RK628D_IsiGetIntegrationTimeLimitsIss;
        pIsiSensor->pIsiGetCurrentExposureIss           = RK628D_IsiGetCurrentExposureIss;
        pIsiSensor->pIsiGetGainIss                      = RK628D_IsiGetGainIss;
        pIsiSensor->pIsiGetGainIncrementIss             = RK628D_IsiGetGainIncrementIss;
        pIsiSensor->pIsiSetGainIss                      = RK628D_IsiSetGainIss;
        pIsiSensor->pIsiGetIntegrationTimeIss           = RK628D_IsiGetIntegrationTimeIss;
        pIsiSensor->pIsiGetIntegrationTimeIncrementIss  = RK628D_IsiGetIntegrationTimeIncrementIss;
        pIsiSensor->pIsiSetIntegrationTimeIss           = RK628D_IsiSetIntegrationTimeIss;
        pIsiSensor->pIsiGetAfpsInfoIss                  = RK628D_IsiGetAfpsInfoIss;

        /* AWB specific functions */
        pIsiSensor->pIsiGetCalibKFactor                 = RK628D_IsiGetCalibKFactor;
        pIsiSensor->pIsiGetCalibPcaMatrix               = RK628D_IsiGetCalibPcaMatrix;
        pIsiSensor->pIsiGetCalibSvdMeanValue            = RK628D_IsiGetCalibSvdMeanValue;
        pIsiSensor->pIsiGetCalibCenterLine              = RK628D_IsiGetCalibCenterLine;
        pIsiSensor->pIsiGetCalibClipParam               = RK628D_IsiGetCalibClipParam;
        pIsiSensor->pIsiGetCalibGlobalFadeParam         = RK628D_IsiGetCalibGlobalFadeParam;
        pIsiSensor->pIsiGetCalibFadeParam               = RK628D_IsiGetCalibFadeParam;
        pIsiSensor->pIsiGetIlluProfile                  = RK628D_IsiGetIlluProfile;
        pIsiSensor->pIsiGetLscMatrixTable               = RK628D_IsiGetLscMatrixTable;

        /* AF functions */
        pIsiSensor->pIsiMdiInitMotoDriveMds             = RK628D_IsiMdiInitMotoDriveMds;
        pIsiSensor->pIsiMdiSetupMotoDrive               = RK628D_IsiMdiSetupMotoDrive;
        pIsiSensor->pIsiMdiFocusSet                     = RK628D_IsiMdiFocusSet;
        pIsiSensor->pIsiMdiFocusGet                     = RK628D_IsiMdiFocusGet;
        pIsiSensor->pIsiMdiFocusCalibrate               = RK628D_IsiMdiFocusCalibrate;

        /* MIPI */
        pIsiSensor->pIsiGetSensorMipiInfoIss            = RK628D_IsiGetSensorMipiInfoIss;

        /* Testpattern */
        pIsiSensor->pIsiActivateTestPattern             = RK628D_IsiActivateTestPattern;
    }
    else
    {
        result = RET_NULL_POINTER;
    }

    TRACE( RK628D_INFO, "%s (exit)\n", __FUNCTION__);

    return ( result );
}

static RESULT RK628D_IsiGetSensorI2cInfo(sensor_i2c_info_t** pdata)
{
    sensor_i2c_info_t* pSensorI2cInfo;

    TRACE( RK628D_INFO, "%s (Enter)\n", __FUNCTION__);
    pSensorI2cInfo = ( sensor_i2c_info_t * )malloc ( sizeof (sensor_i2c_info_t) );

    if ( pSensorI2cInfo == NULL )
    {
        TRACE( RK628D_ERROR,  "%s: Can't allocate ov14825 context\n",  __FUNCTION__ );
        return ( RET_OUTOFMEM );
    }
    MEMSET( pSensorI2cInfo, 0, sizeof( sensor_i2c_info_t ) );

    pSensorI2cInfo->i2c_addr = RK628D_SLAVE_ADDR;
    pSensorI2cInfo->i2c_addr2 = RK628D_SLAVE_ADDR2;
    pSensorI2cInfo->soft_reg_addr = RK628D_SOFTWARE_RST_REG;
    pSensorI2cInfo->soft_reg_value = RK628D_SOFTWARE_RST_VAL;
    pSensorI2cInfo->reg_size = RK628D_I2C_NR_REG_ADR_BYTES;
    pSensorI2cInfo->value_size = RK628D_I2C_NR_REG_DAT_BYTES;

    {
        IsiSensorCaps_t Caps;
        sensor_caps_t *pCaps;
        uint32_t lanes,i;

        for (i=0; i<3; i++) {
            lanes = (1<<i);
            ListInit(&pSensorI2cInfo->lane_res[i]);
            if (g_suppoted_mipi_lanenum_type & lanes) {
                Caps.Index = 0;
                while(RK628D_IsiGetCapsIssInternal(&Caps,lanes)==RET_SUCCESS) {
                    pCaps = malloc(sizeof(sensor_caps_t));
                    if (pCaps != NULL) {
                        memcpy(&pCaps->caps,&Caps,sizeof(IsiSensorCaps_t));
                        ListPrepareItem(pCaps);
                        ListAddTail(&pSensorI2cInfo->lane_res[i], pCaps);
                    }
                    Caps.Index++;
                }
            }
        }
    }

    ListInit(&pSensorI2cInfo->chipid_info);

    sensor_chipid_info_t* pChipIDInfo_H = (sensor_chipid_info_t *) malloc( sizeof(sensor_chipid_info_t) );
    if ( !pChipIDInfo_H )
    {
        return RET_OUTOFMEM;
    }
    MEMSET( pChipIDInfo_H, 0, sizeof(*pChipIDInfo_H) );
    pChipIDInfo_H->chipid_reg_addr = RK628D_CHIP_ID_HIGH_BYTE;
    pChipIDInfo_H->chipid_reg_value = RK628D_CHIP_ID_HIGH_BYTE_DEFAULT;
    ListPrepareItem( pChipIDInfo_H );
    ListAddTail( &pSensorI2cInfo->chipid_info, pChipIDInfo_H );

    pSensorI2cInfo->sensor_drv_version = CONFIG_SENSOR_DRV_VERSION;
    *pdata = pSensorI2cInfo;
    TRACE( RK628D_INFO, "%s (Exit)\n", __FUNCTION__);

    return RET_SUCCESS;
}

/******************************************************************************
 * See header file for detailed comment.
 *****************************************************************************/

/*****************************************************************************/
/**
 */
/*****************************************************************************/
IsiCamDrvConfig_t IsiCamDrvConfig =
{
    0,
    RK628D_IsiGetSensorIss,
    {
        0,                      /**< IsiSensor_t.pszName */
        0,                      /**< IsiSensor_t.pRegisterTable */
        0,                      /**< IsiSensor_t.pIsiSensorCaps */
        0,			/**< IsiSensor_t.pIsiGetSensorIsiVer_t>*/   //oyyf add
        0,                      /**< IsiSensor_t.pIsiGetSensorTuningXmlVersion_t>*/   //oyyf add
        0,                      /**< IsiSensor_t.pIsiWhiteBalanceIlluminationChk>*/   //ddl@rock-chips.com
        0,                      /**< IsiSensor_t.pIsiWhiteBalanceIlluminationSet>*/   //ddl@rock-chips.com
        0,                      /**< IsiSensor_t.pIsiCheckOTPInfo>*/  //zyc
        0,			/**< IsiSensor_t.pIsiSetSensorOTPInfo>*/  //zyl
        0,			/**< IsiSensor_t.pIsiEnableSensorOTP>*/  //zyl
        0,                      /**< IsiSensor_t.pIsiCreateSensorIss */
        0,                      /**< IsiSensor_t.pIsiReleaseSensorIss */
        0,                      /**< IsiSensor_t.pIsiGetCapsIss */
        0,                      /**< IsiSensor_t.pIsiSetupSensorIss */
        0,                      /**< IsiSensor_t.pIsiChangeSensorResolutionIss */
        0,                      /**< IsiSensor_t.pIsiSensorSetStreamingIss */
        0,                      /**< IsiSensor_t.pIsiSensorSetPowerIss */
        0,                      /**< IsiSensor_t.pIsiCheckSensorConnectionIss */
        0,                      /**< IsiSensor_t.pIsiGetSensorRevisionIss */
        0,                      /**< IsiSensor_t.pIsiRegisterReadIss */
        0,                      /**< IsiSensor_t.pIsiRegisterWriteIss */
        0,                      /**< IsiSensor_t.pIsiIsEvenFieldIss */

        0,                      /**< IsiSensor_t.pIsiExposureControlIss */
        0,                      /**< IsiSensor_t.pIsiGetGainLimitsIss */
        0,                      /**< IsiSensor_t.pIsiGetIntegrationTimeLimitsIss */
        0,                      /**< IsiSensor_t.pIsiGetCurrentExposureIss */
        0,                      /**< IsiSensor_t.pIsiGetGainIss */
        0,                      /**< IsiSensor_t.pIsiGetGainIncrementIss */
        0,                      /**< IsiSensor_t.pIsiSetGainIss */
        0,                      /**< IsiSensor_t.pIsiGetIntegrationTimeIss */
        0,                      /**< IsiSensor_t.pIsiGetIntegrationTimeIncrementIss */
        0,                      /**< IsiSensor_t.pIsiSetIntegrationTimeIss */
        0,                      /**< IsiSensor_t.pIsiGetResolutionIss */
        0,                      /**< IsiSensor_t.pIsiGetAfpsInfoIss */

        0,                      /**< IsiSensor_t.pIsiGetCalibKFactor */
        0,                      /**< IsiSensor_t.pIsiGetCalibPcaMatrix */
        0,                      /**< IsiSensor_t.pIsiGetCalibSvdMeanValue */
        0,                      /**< IsiSensor_t.pIsiGetCalibCenterLine */
        0,                      /**< IsiSensor_t.pIsiGetCalibClipParam */
        0,                      /**< IsiSensor_t.pIsiGetCalibGlobalFadeParam */
        0,                      /**< IsiSensor_t.pIsiGetCalibFadeParam */
        0,                      /**< IsiSensor_t.pIsiGetIlluProfile */
        0,                      /**< IsiSensor_t.pIsiGetLscMatrixTable */

        0,                      /**< IsiSensor_t.pIsiMdiInitMotoDriveMds */
        0,                      /**< IsiSensor_t.pIsiMdiSetupMotoDrive */
        0,                      /**< IsiSensor_t.pIsiMdiFocusSet */
        0,                      /**< IsiSensor_t.pIsiMdiFocusGet */
        0,                      /**< IsiSensor_t.pIsiMdiFocusCalibrate */

        0,                      /**< IsiSensor_t.pIsiGetSensorMipiInfoIss */

        0,                      /**< IsiSensor_t.pIsiActivateTestPattern */
    },
    RK628D_IsiGetSensorI2cInfo,
};




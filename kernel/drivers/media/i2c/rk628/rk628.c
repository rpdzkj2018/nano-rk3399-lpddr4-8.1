// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2020 Rockchip Electronics Co. Ltd.
 *
 * Author: Shunqing Chen <csq@rock-chips.com>
 */

#include <asm-generic/uaccess.h>
#include <linux/debugfs.h>
#include <linux/i2c.h>
#include <linux/module.h>

#include "rk628_csi.h"
#include "rk628.h"
#include "rk628_cru.h"
#include "rk628_combrxphy.h"
#include "rk628_combtxphy.h"

struct i2c_client *rk628_i2c = NULL;

static int i2c_read(struct i2c_client *i2c, u32 reg, u32 *val)
{
	int ret;
	struct i2c_msg msg[] = {
		{
			.addr   = i2c->addr,
			.flags  = 0,
			.len    = 4,
			.buf    = (u8 *)&reg,
		},
		{
			.addr   = i2c->addr,
			.flags  = I2C_M_RD,
			.len    = 4,
			.buf    = (u8 *)val,
		},
	};

	ret = i2c_transfer(i2c->adapter, msg, 2);
	if (ret < 0) {
		dev_err(&i2c->dev, "Failed reading register 0x%04x!\n", reg);
		return ret;
	}

	return 0;

}

static int i2c_write(struct i2c_client *i2c, u32 reg, u32 val)
{
	struct i2c_msg msg;
	struct {
		u32 reg;
		u32 val;
	} __packed buf;
	int ret;

	buf.reg = reg;
	buf.val = val;

	msg.addr        = i2c->addr;
	msg.flags       = 0;
	msg.len         = 8;
	msg.buf         = (u8 *)&buf;

	ret = i2c_transfer(i2c->adapter, &msg, 1);
	if (ret < 0) {
		dev_err(&i2c->dev, "Failed writing register 0x%04x!\n", reg);
		return ret;
	}

	return 0;
}

void rk628_i2c_write(u32 reg, u32 val)
{
	if (!rk628_i2c)
		return;

	i2c_write(rk628_i2c, reg, val);
}
EXPORT_SYMBOL_GPL(rk628_i2c_write);

int rk628_i2c_read(u32 reg, u32 *val)
{
	if (!rk628_i2c)
		return -1;

	return i2c_read(rk628_i2c, reg, val);
}
EXPORT_SYMBOL_GPL(rk628_i2c_read);

void rk628_i2c_update_bits(u32 reg, u32 mask, u32 val)
{
	u32 orig, tmp;

	if (!rk628_i2c)
		return;

	i2c_read(rk628_i2c, reg, &orig);
	tmp = orig & ~mask;
	tmp |= val & mask;
	i2c_write(rk628_i2c, reg, tmp);
}
EXPORT_SYMBOL_GPL(rk628_i2c_update_bits);

static void rk628_grf_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_grf==== \n");
	for (reg = GRF_SYSTEM_CON0; reg < GRF_SOC_VERSION + 4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}

	seq_puts(s, "\n");
}

static void rk628_cru_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_cru==== \n");
	for (reg = 0xc0000; reg < 0xc0214; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}

	seq_puts(s, "\n");
}

static void rk628_hdmirx_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_hdmirx==== \n");
	for (reg = 0x30000; reg <= 0x30000; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30038; reg <= 0x30038; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30080; reg <= 0x30084; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30090; reg <= 0x3009c; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x300a4; reg <= 0x300a4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x300c0; reg <= 0x300c4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x300d4; reg <= 0x300d4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x300e0; reg <= 0x300fc; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30140; reg <= 0x30150; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30158; reg <= 0x3015c; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30164; reg <= 0x30170; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x3017c; reg <= 0x30180; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30200; reg <= 0x30200; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30208; reg <= 0x30208; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30214; reg <= 0x30214; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30240; reg <= 0x30244; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30254; reg <= 0x30264; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x3027c; reg <= 0x3027c; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30280; reg <= 0x30284; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30310; reg <= 0x30310; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x3033c; reg <= 0x30340; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30390; reg <= 0x30394; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x303a4; reg <= 0x303a4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x303c0; reg <= 0x303c8; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30800; reg <= 0x3080c; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30824; reg <= 0x30828; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30860; reg <= 0x30860; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30fb0; reg <= 0x30fb4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30fc8; reg <= 0x30fcc; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30fe0; reg <= 0x30fe4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x30ff4; reg <= 0x30ff4; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}

	seq_puts(s, "\n");
}

static void rk628_combrxphy_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_combrxphy==== \n");
	for (reg = 0x16600; reg <= 0x1665b; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x166a0; reg <= 0x166db; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x166f0; reg <= 0x166ff; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = 0x16700; reg <= 0x16790; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	seq_puts(s, "\n");
}

static void rk628_combtxphy_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_combtxphy==== \n");
	for (reg = 0x90000; reg < 0x9002c; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}

	seq_puts(s, "\n");
}

static void rk628_csi_registers_dump(struct seq_file *s)
{
	uint32_t reg, val;

	seq_puts(s, "====rk628_csi==== \n");
	for (reg = CSITX_CONFIG_DONE; reg < CSITX_CSITX_VERSION; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = CSITX_SYS_CTRL0_IMD; reg < CSITX_TIMING_HPW_PADDING_NUM; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = CSITX_VOP_PATH_CTRL; reg < CSITX_VOP_PATH_CTRL; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = CSITX_VOP_PATH_PKT_CTRL; reg < CSITX_VOP_PATH_PKT_CTRL; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = CSITX_CSITX_STATUS0; reg < CSITX_LPDT_DATA_IMD; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	for (reg = CSITX_DPHY_CTRL; reg < CSITX_DPHY_CTRL; reg += 4) {
		rk628_i2c_read(reg, &val);
		seq_printf(s, "0x%05x: %08x \n", reg, val);
	}
	seq_puts(s, "\n");
}


static void rk628_registers_dump(struct seq_file *s)
{
	rk628_grf_registers_dump(s);
	rk628_cru_registers_dump(s);
	rk628_combtxphy_registers_dump(s);
	rk628_combrxphy_registers_dump(s);
	rk628_hdmirx_registers_dump(s);
	rk628_csi_registers_dump(s);
}

struct dentry *debugfs_dir;

static int rk628_regs_show(struct seq_file *s, void *v)
{
	seq_puts(s, "/*******rk628 registers read******/\n");
	rk628_registers_dump(s);
	return 0;
}

static int rk628_regs_open(struct inode *inode, struct file *file)
{
	return single_open(file, rk628_regs_show, inode->i_private);
}

static ssize_t
rk628_regs_write(struct file *file, const char __user *buf,
		 size_t count, loff_t *ppos)
{
	u32 reg, val;
	char kbuf[25];

	if (copy_from_user(kbuf, buf, count))
		return -EFAULT;
	if (sscanf(kbuf, "%x%x", &reg, &val) == -1)
		return -EFAULT;
	dev_info(&rk628_i2c->dev, "/*******rk628 register config******/\n");
	dev_info(&rk628_i2c->dev, "\n reg=%x val=%x\n", reg, val);
	rk628_i2c_write(reg, val);

	return count;
}

static const struct file_operations rk628_regs_fops = {
	.owner = THIS_MODULE,
	.open = rk628_regs_open,
	.read = seq_read,
	.write = rk628_regs_write,
	.llseek = seq_lseek,
	.release = single_release,
};

void rk628_register_debugfs(struct device *dev)
{
	debugfs_dir = debugfs_create_dir("rk628", NULL);
	if (IS_ERR(debugfs_dir)) {
		dev_err(dev, "failed to create debugfs dir!\n");
		return;
	}
	debugfs_create_file("regs", 0400, debugfs_dir,
			    NULL, &rk628_regs_fops);
}

MODULE_AUTHOR("Shunqing Chen <csq@rock-chips.com>");
MODULE_DESCRIPTION("Rockchip RK628 driver");
MODULE_LICENSE("GPL v2");

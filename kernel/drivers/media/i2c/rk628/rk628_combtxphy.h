/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2020 Rockchip Electronics Co. Ltd.
 *
 * Author: Shunqing Chen csq@rock-chips.com>
 */

#ifndef _RK628_COMBTXPHY_H
#define _RK628_COMBTXPHY_H

enum phy_mode {
	PHY_MODE_INVALID,
	PHY_MODE_VIDEO_MIPI,
	PHY_MODE_VIDEO_LVDS,
	PHY_MODE_VIDEO_GVI,
};

void rk628_combtxphy_set_mode(enum phy_mode mode);
void rk628_combtxphy_set_bus_width(uint32_t bus_width);
uint32_t rk628_combtxphy_get_bus_width(void);
void rk628_combtxphy_power_on(void);
void rk628_combtxphy_power_off(void);

#endif

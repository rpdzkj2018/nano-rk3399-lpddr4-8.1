/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2020 Rockchip Electronics Co. Ltd.
 *
 * Author: Shunqing Chen csq@rock-chips.com>
 */

#ifndef _RK628_COMBRXPHY_H
#define _RK628_COMBRXPHY_H

int rk628_combrxphy_power_on(int f);
int rk628_combrxphy_power_off(void);

#endif

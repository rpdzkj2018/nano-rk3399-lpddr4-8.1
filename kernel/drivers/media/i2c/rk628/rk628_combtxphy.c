// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2020 Rockchip Electronics Co. Ltd.
 *
 * Author: Shunqing Chen <csq@rock-chips.com>
 */

#include <linux/delay.h>
#include "rk628.h"
#include "rk628_combtxphy.h"

#define REG(x)			((x) + 0x90000)

#define COMBTXPHY_CON0		REG(0x0000)
#define SW_TX_IDLE_MASK		GENMASK(29, 20)
#define SW_TX_IDLE(x)		UPDATE(x, 29, 20)
#define SW_TX_PD_MASK		GENMASK(17, 8)
#define SW_TX_PD(x)		UPDATE(x, 17, 8)
#define SW_BUS_WIDTH_MASK	GENMASK(6, 5)
#define SW_BUS_WIDTH_7BIT	UPDATE(0x3, 6, 5)
#define SW_BUS_WIDTH_8BIT	UPDATE(0x2, 6, 5)
#define SW_BUS_WIDTH_9BIT	UPDATE(0x1, 6, 5)
#define SW_BUS_WIDTH_10BIT	UPDATE(0x0, 6, 5)
#define SW_PD_PLL_MASK		BIT(4)
#define SW_PD_PLL		BIT(4)
#define SW_GVI_LVDS_EN_MASK	BIT(3)
#define SW_GVI_LVDS_EN		BIT(3)
#define SW_MIPI_DSI_EN_MASK	BIT(2)
#define SW_MIPI_DSI_EN		BIT(2)
#define SW_MODULEB_EN_MASK	BIT(1)
#define SW_MODULEB_EN		BIT(1)
#define SW_MODULEA_EN_MASK	BIT(0)
#define SW_MODULEA_EN		BIT(0)
#define COMBTXPHY_CON1		REG(0x0004)
#define COMBTXPHY_CON2		REG(0x0008)
#define COMBTXPHY_CON3		REG(0x000c)
#define COMBTXPHY_CON4		REG(0x0010)
#define COMBTXPHY_CON5		REG(0x0014)
#define SW_RATE(x)		UPDATE(x, 26, 24)
#define SW_REF_DIV(x)		UPDATE(x, 20, 16)
#define SW_PLL_FB_DIV(x)	UPDATE(x, 14, 10)
#define SW_PLL_FRAC_DIV(x)	UPDATE(x, 9, 0)
#define COMBTXPHY_CON6		REG(0x0018)
#define COMBTXPHY_CON7		REG(0x001c)
#define SW_TX_RTERM_MASK	GENMASK(22, 20)
#define SW_TX_RTERM(x)		UPDATE(x, 22, 20)
#define SW_TX_MODE_MASK		GENMASK(17, 16)
#define SW_TX_MODE(x)		UPDATE(x, 17, 16)
#define SW_TX_CTL_CON5_MASK	BIT(10)
#define SW_TX_CTL_CON5(x)	UPDATE(x, 10, 10)
#define SW_TX_CTL_CON4_MASK	GENMASK(9, 8)
#define SW_TX_CTL_CON4(x)	UPDATE(x, 9, 8)
#define COMBTXPHY_CON8		REG(0x0020)
#define COMBTXPHY_CON9		REG(0x0024)
#define SW_DSI_FSET_EN_MASK	BIT(29)
#define SW_DSI_FSET_EN		BIT(29)
#define SW_DSI_RCAL_EN_MASK	BIT(28)
#define SW_DSI_RCAL_EN		BIT(28)
#define COMBTXPHY_CON10		REG(0x0028)
#define TX9_CKDRV_EN		BIT(9)
#define TX8_CKDRV_EN		BIT(8)
#define TX7_CKDRV_EN		BIT(7)
#define TX6_CKDRV_EN		BIT(6)
#define TX5_CKDRV_EN		BIT(5)
#define TX4_CKDRV_EN		BIT(4)
#define TX3_CKDRV_EN		BIT(3)
#define TX2_CKDRV_EN		BIT(2)
#define TX1_CKDRV_EN		BIT(1)
#define TX0_CKDRV_EN		BIT(0)

struct rk628_combtxphy {
	enum phy_mode mode;
	unsigned int flags;
	uint8_t ref_div;
	uint8_t fb_div;
	uint16_t frac_div;
	uint8_t rate_div;
	uint32_t bus_width;
} gcombtxphy;

static struct rk628_combtxphy *combtxphy = &gcombtxphy;

void rk628_combtxphy_set_bus_width(uint32_t bus_width)
{
	combtxphy->bus_width = bus_width;
}

uint32_t rk628_combtxphy_get_bus_width(void)
{
	return combtxphy->bus_width;
}

static void rk628_combtxphy_dsi_power_on(void)
{
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_BUS_WIDTH_MASK |
			      SW_GVI_LVDS_EN_MASK |
			      SW_MIPI_DSI_EN_MASK,
			      SW_BUS_WIDTH_8BIT | SW_MIPI_DSI_EN);

	if (combtxphy->flags & COMBTXPHY_MODULEA_EN)
		rk628_i2c_update_bits(COMBTXPHY_CON0, SW_MODULEA_EN_MASK,
				      SW_MODULEA_EN);
	if (combtxphy->flags & COMBTXPHY_MODULEB_EN)
		rk628_i2c_update_bits(COMBTXPHY_CON0, SW_MODULEB_EN_MASK,
				      SW_MODULEB_EN);

	rk628_i2c_write(COMBTXPHY_CON5, SW_REF_DIV(combtxphy->ref_div - 1) |
			SW_PLL_FB_DIV(combtxphy->fb_div) |
			SW_PLL_FRAC_DIV(combtxphy->frac_div) |
			SW_RATE(combtxphy->rate_div / 2));
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_PD_PLL, 0);
	msleep(1);
	rk628_i2c_update_bits(COMBTXPHY_CON9, SW_DSI_FSET_EN_MASK |
			      SW_DSI_RCAL_EN_MASK, SW_DSI_FSET_EN |
			      SW_DSI_RCAL_EN);
	msleep(1);
}

static void rk628_combtxphy_lvds_power_on(void)
{
	rk628_i2c_update_bits(COMBTXPHY_CON7, SW_TX_MODE_MASK, SW_TX_MODE(3));
	rk628_i2c_write(COMBTXPHY_CON10, TX7_CKDRV_EN | TX2_CKDRV_EN);
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_BUS_WIDTH_MASK |
			      SW_GVI_LVDS_EN_MASK | SW_MIPI_DSI_EN_MASK,
			      SW_BUS_WIDTH_7BIT | SW_GVI_LVDS_EN);

	if (combtxphy->flags & COMBTXPHY_MODULEA_EN)
		rk628_i2c_update_bits(COMBTXPHY_CON0, SW_MODULEA_EN_MASK,
				      SW_MODULEA_EN);
	if (combtxphy->flags & COMBTXPHY_MODULEB_EN)
		rk628_i2c_update_bits(COMBTXPHY_CON0, SW_MODULEB_EN_MASK,
				      SW_MODULEB_EN);

	rk628_i2c_write(COMBTXPHY_CON5, SW_REF_DIV(combtxphy->ref_div - 1) |
			SW_PLL_FB_DIV(combtxphy->fb_div) |
			SW_PLL_FRAC_DIV(combtxphy->frac_div) |
			SW_RATE(combtxphy->rate_div / 2));
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_PD_PLL | SW_TX_PD_MASK, 0);
	msleep(1);
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_TX_IDLE_MASK, 0);
}

static void rk628_combtxphy_gvi_power_on(void)
{
	rk628_i2c_write(COMBTXPHY_CON5, SW_REF_DIV(combtxphy->ref_div - 1) |
			SW_PLL_FB_DIV(combtxphy->fb_div) |
			SW_PLL_FRAC_DIV(combtxphy->frac_div) |
			SW_RATE(combtxphy->rate_div / 2));
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_BUS_WIDTH_MASK |
			      SW_GVI_LVDS_EN_MASK | SW_MIPI_DSI_EN_MASK |
			      SW_MODULEB_EN_MASK | SW_MODULEA_EN_MASK,
			      SW_BUS_WIDTH_10BIT | SW_GVI_LVDS_EN |
			      SW_MODULEB_EN | SW_MODULEA_EN);
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_PD_PLL | SW_TX_PD_MASK, 0);
	msleep(1);
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_TX_IDLE_MASK, 0);
}

void rk628_combtxphy_set_mode(enum phy_mode mode)
{
	unsigned int fvco, frac_rate, fin = 24;

	switch (mode) {
	case PHY_MODE_VIDEO_MIPI:
	{
		int bus_width = rk628_combtxphy_get_bus_width();
		unsigned int fhsc = bus_width >> 8;
		unsigned int flags = bus_width & 0xff;

		fhsc = fin * (fhsc / fin);
		if (fhsc < 80 || fhsc > 1500)
			return;
		else if (fhsc < 375)
			combtxphy->rate_div = 4;
		else if (fhsc < 750)
			combtxphy->rate_div = 2;
		else
			combtxphy->rate_div = 1;

		combtxphy->flags = flags;

		fvco = fhsc * 2 * combtxphy->rate_div;
		combtxphy->ref_div = 1;
		combtxphy->fb_div = fvco / 8 / fin;
		frac_rate = fvco - (fin * 8 * combtxphy->fb_div);
		if (frac_rate) {
			frac_rate <<= 10;
			frac_rate /= fin * 8;
			combtxphy->frac_div = frac_rate;
		} else {
			combtxphy->frac_div = 0;
		}

		fvco = fin * (1024 * combtxphy->fb_div + combtxphy->frac_div);
		fvco *= 8;
		fvco = DIV_ROUND_UP(fvco, 1024 * combtxphy->ref_div);
		fhsc = fvco / 2 / combtxphy->rate_div;
		combtxphy->bus_width = fhsc;

		break;
	}
	case PHY_MODE_VIDEO_LVDS:
	{
		int bus_width = rk628_combtxphy_get_bus_width();
		unsigned int flags = bus_width & 0xff;
		unsigned int rate = (bus_width >> 8) * 7;

		combtxphy->flags = flags;
		combtxphy->ref_div = 1;
		combtxphy->fb_div = 14;
		combtxphy->frac_div = 0;

		if (rate < 500)
			combtxphy->rate_div = 4;
		else if (rate < 1000)
			combtxphy->rate_div = 2;
		else
			combtxphy->rate_div = 1;
		break;
	}
	case PHY_MODE_VIDEO_GVI:
	{
		unsigned int fhsc = rk628_combtxphy_get_bus_width() & 0xfff;

		if (fhsc < 500 || fhsc > 4000)
			return;
		else if (fhsc < 1000)
			combtxphy->rate_div = 4;
		else if (fhsc < 2000)
			combtxphy->rate_div = 2;
		else
			combtxphy->rate_div = 1;
		fvco = fhsc * combtxphy->rate_div;

		combtxphy->ref_div = 1;
		combtxphy->fb_div = fvco / 8 / fin;
		frac_rate = fvco - (fin * 8 * combtxphy->fb_div);

		if (frac_rate) {
			frac_rate <<= 10;
			frac_rate /= fin * 8;
			combtxphy->frac_div = frac_rate;
		} else {
			combtxphy->frac_div = 0;
		}

		fvco = fin * (1024 * combtxphy->fb_div + combtxphy->frac_div);
		fvco *= 8;
		fvco /= 1024 * combtxphy->ref_div;
		fhsc = fvco / combtxphy->rate_div;
		combtxphy->bus_width = fhsc;
		break;
	}
	default:
		break;
	}

	combtxphy->mode = mode;
}

void rk628_combtxphy_power_on(void)
{
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_TX_IDLE_MASK | SW_TX_PD_MASK |
			      SW_PD_PLL_MASK, SW_TX_IDLE(0x3ff) |
			      SW_TX_PD(0x3ff) | SW_PD_PLL);

	switch (combtxphy->mode) {
	case PHY_MODE_VIDEO_MIPI:

		rk628_i2c_update_bits(GRF_POST_PROC_CON,
				      SW_TXPHY_REFCLK_SEL_MASK,
				      SW_TXPHY_REFCLK_SEL(0));
		rk628_combtxphy_dsi_power_on();
		break;
	case PHY_MODE_VIDEO_LVDS:
		rk628_i2c_update_bits(GRF_POST_PROC_CON,
				      SW_TXPHY_REFCLK_SEL_MASK,
				      SW_TXPHY_REFCLK_SEL(1));
		rk628_combtxphy_lvds_power_on();
		break;
	case PHY_MODE_VIDEO_GVI:
		rk628_i2c_update_bits(GRF_POST_PROC_CON,
				      SW_TXPHY_REFCLK_SEL_MASK,
				      SW_TXPHY_REFCLK_SEL(0));
		rk628_combtxphy_gvi_power_on();
		break;
	default:
		break;
	}
}

void rk628_combtxphy_power_off(void)
{
	rk628_i2c_update_bits(COMBTXPHY_CON0, SW_TX_IDLE_MASK | SW_TX_PD_MASK |
			      SW_PD_PLL_MASK | SW_MODULEB_EN_MASK |
			      SW_MODULEA_EN_MASK, SW_TX_IDLE(0x3ff) |
			      SW_TX_PD(0x3ff) | SW_PD_PLL);
}

MODULE_AUTHOR("Shunqing Chen <csq@rock-chips.com>");
MODULE_DESCRIPTION("Rockchip RK628 GVI/LVDS/MIPI Combo TX PHY driver");
MODULE_LICENSE("GPL v2");

// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2020 Rockchip Electronics Co. Ltd.
 *
 * Author: Shunqing Chen <csq@rock-chips.com>
 */

#include <linux/delay.h>
#include "rk628.h"
#include "rk628_cru.h"

#define RK628_CRU_BASE		0xc0000
#define CRU_CPLL_CON00		0x0000
#define PLL_BYPASS_MASK		GENMASK(15, 15)
#define PLL_BYPASS		(1 << 15)
#define PLL_BYPASS_SHIFT	15
#define PLL_POSTDIV1_MASK	GENMASK(14, 12)
#define PLL_POSTDIV1(x)		((x) << 12)
#define PLL_POSTDIV1_SHIFT	12
#define PLL_FBDIV_MASK		GENMASK(11, 0)
#define PLL_FBDIV(x)		((x) << 0)
#define PLL_FBDIV_SHIFT		0
#define CRU_CPLL_CON01		0x0004
#define PLL_PD_MASK		GENMASK(13, 13)
#define PLL_PD			((1) << 13)
#define PLL_DSMPD_MASK		GENMASK(12, 12)
#define PLL_DSMPD(x)		((x) << 12)
#define PLL_DSMPD_SHIFT		12
#define PLL_LOCK		GENMASK(10, 10)
#define PLL_POSTDIV2_MASK	GENMASK(8, 6)
#define PLL_POSTDIV2(x)		((x) << 6)
#define PLL_POSTDIV2_SHIFT	6
#define PLL_REFDIV_MASK		GENMASK(5, 0)
#define PLL_REFDIV(x)		((x) << 0)
#define PLL_REFDIV_SHIFT	0
#define CRU_CPLL_CON02		0x0008
#define PLL_FRAC_MASK		GENMASK(23, 0)
#define PLL_FRAC(x)		((x) << 0)
#define PLL_FRAC_SHIFT		0
#define CRU_CPLL_CON03		0x000c
#define CRU_CPLL_CON04		0x0010
#define CRU_GPLL_CON00		0x0020
#define CRU_GPLL_CON01		0x0024
#define CRU_GPLL_CON02		0x0028
#define CRU_GPLL_CON03		0x002c
#define CRU_GPLL_CON04		0x0030
#define CRU_MODE_CON00		0x0060
#define CLK_GPLL_MODE_MASK	GENMASK(2, 2)
#define CLK_GPLL_MODE_GPLL	(1 << 2)
#define CLK_GPLL_MODE_OSC	(0 << 2)
#define CLK_CPLL_MODE_MASK	GENMASK(0, 0)
#define CLK_CPLL_MODE_CPLL	(1 << 0)
#define CLK_CPLL_MODE_OSC	(0 << 0)
#define CRU_CLKSEL_CON00	0x0080
#define CRU_CLKSEL_CON01	0x0084
#define CRU_CLKSEL_CON02	0x0088
#define SCLK_VOP_SEL_MASK	GENMASK(9, 9)
#define SCLK_VOP_SEL_GPLL	(1 << 9)
#define SCLK_VOP_SEL_CPLL	(0 << 9)
#define CLK_RX_READ_SEL_MASK	GENMASK(8, 8)
#define CLK_RX_READ_SEL_GPLL	(1 << 8)
#define CLK_RX_READ_SEL_CPLL	(0 << 8)
#define CRU_CLKSEL_CON03	0x008c
#define CRU_CLKSEL_CON04	0x0090
#define CRU_CLKSEL_CON05	0x0094
#define CLK_HDMIRX_AUD_SEL	BIT(15)
#define CRU_CLKSEL_CON06	0x0098
#define SCLK_UART_SEL_MASK	GENMASK(15, 14)
#define SCLK_UART_SEL_OSC	(2 << 14)
#define SCLK_UART_SEL_UART_FRAC	(1 << 14)
#define SCLK_UART_SEL_UART_SRC	(0 << 14)
#define CRU_CLKSEL_CON07	0x009c
#define CRU_CLKSEL_CON08	0x00a0
#define CRU_CLKSEL_CON09	0x00a4
#define CRU_CLKSEL_CON10	0x00a8
#define CRU_CLKSEL_CON11	0x00ac
#define CRU_CLKSEL_CON12	0x00b0
#define CRU_CLKSEL_CON13	0x00b4
#define CRU_CLKSEL_CON14	0x00b8
#define CRU_CLKSEL_CON15	0x00bc
#define CRU_CLKSEL_CON16	0x00c0
#define CRU_CLKSEL_CON17	0x00c4
#define CRU_CLKSEL_CON18	0x00c8
#define CRU_CLKSEL_CON20	0x00d0
#define CRU_CLKSEL_CON21	0x00d4
#define CLK_UART_SRC_SEL_MASK	GENMASK(15, 15)
#define CLK_UART_SRC_SEL_GPLL	(1 << 15)
#define CLK_UART_SRC_SEL_CPLL	(0 << 15)
#define CLK_UART_SRC_DIV_MASK	GENMASK(12, 8)
#define CLK_UART_SRC_DIV_SHIFT	8
#define CRU_GATE_CON00		0x0180
#define CRU_GATE_CON01		0x0184
#define CRU_GATE_CON02		0x0188
#define CRU_GATE_CON03		0x018c
#define CRU_GATE_CON04		0x0190
#define CRU_GATE_CON05		0x0194
#define CRU_SOFTRST_CON00	0x0200
#define CRU_SOFTRST_CON01	0x0204
#define CRU_SOFTRST_CON02	0x0208
#define CRU_SOFTRST_CON04	0x0210

#define REFCLK_RATE		24000000UL
#define MIN_FREF_RATE		10000000UL
#define MAX_FREF_RATE		800000000UL
#define MIN_FREFDIV_RATE	1000000UL
#define MAX_FREFDIV_RATE	100000000UL
#define MIN_FVCO_RATE		600000000UL
#define MAX_FVCO_RATE		1600000000UL
#define MIN_FOUTPOSTDIV_RATE	12000000UL
#define MAX_FOUTPOSTDIV_RATE	1600000000UL

static inline void cru_write(uint32_t reg, uint32_t val)
{
	rk628_i2c_write(RK628_CRU_BASE + reg, val);
}

static inline uint32_t cru_read(uint32_t reg)
{
	u32 val;

	rk628_i2c_read(RK628_CRU_BASE + reg, &val);

	return val;
}

static inline void cru_update_bits(uint32_t reg, uint32_t mask, uint32_t val)
{
	val &= mask;
	val |= mask << 16;
	cru_write(reg, val);
}

static void rational_best_approximation(unsigned long given_numerator,
					unsigned long given_denominator,
					unsigned long max_numerator,
					unsigned long max_denominator,
					unsigned long *best_numerator,
					unsigned long *best_denominator)
{
	unsigned long n, d, n0, d0, n1, d1;

	n = given_numerator;
	d = given_denominator;
	n0 = d1 = 0;
	n1 = d0 = 1;
	for (;;) {
		unsigned long t, a;
		if ((n1 > max_numerator) || (d1 > max_denominator)) {
			n1 = n0;
			d1 = d0;
			break;
		}
		if (d == 0)
			break;
		t = d;
		a = n / d;
		d = n % d;
		n = t;
		t = n0 + a * n1;
		n0 = n1;
		n1 = t;
		t = d0 + a * d1;
		d0 = d1;
		d1 = t;
	}
	*best_numerator = n1;
	*best_denominator = d1;
}

static unsigned long cru_clk_get_rate_pll(unsigned int id)
{
	unsigned long parent_rate = REFCLK_RATE;
	uint32_t postdiv1, fbdiv, dsmpd, postdiv2, refdiv, frac, bypass;
	uint32_t con0, con1, con2;
	uint64_t foutvco, foutpostdiv;
	uint32_t reg, val;

	val = cru_read(CRU_MODE_CON00);
	if (id == CGU_CLK_CPLL) {
		if (!(val & CLK_CPLL_MODE_MASK))
			return parent_rate;

		reg = CRU_CPLL_CON00;
	} else {
		if (!(val & CLK_GPLL_MODE_MASK))
			return parent_rate;

		reg = CRU_GPLL_CON00;
	}

	con0 = cru_read(reg + CRU_CPLL_CON00);
	con1 = cru_read(reg + CRU_CPLL_CON01);
	con2 = cru_read(reg + CRU_CPLL_CON02);

	bypass = (con0 & PLL_BYPASS_MASK) >> PLL_BYPASS_SHIFT;
	postdiv1 = (con0 & PLL_POSTDIV1_MASK) >> PLL_POSTDIV1_SHIFT;
	fbdiv = (con0 & PLL_FBDIV_MASK) >> PLL_FBDIV_SHIFT;
	dsmpd = (con1 & PLL_DSMPD_MASK) >> PLL_DSMPD_SHIFT;
	postdiv2 = (con1 & PLL_POSTDIV2_MASK) >> PLL_POSTDIV2_SHIFT;
	refdiv = (con1 & PLL_REFDIV_MASK) >> PLL_REFDIV_SHIFT;
	frac = (con2 & PLL_FRAC_MASK) >> PLL_FRAC_SHIFT;

	if (bypass)
		return parent_rate;

	foutvco = parent_rate * fbdiv;
	do_div(foutvco, refdiv);

	if (!dsmpd) {
		uint64_t frac_rate = (uint64_t)parent_rate * frac;

		do_div(frac_rate, refdiv);
		foutvco += frac_rate >> 24;
	}

	foutpostdiv = foutvco;
	do_div(foutpostdiv, postdiv1);
	do_div(foutpostdiv, postdiv2);

	return foutpostdiv;
}

static unsigned long cru_clk_set_rate_pll(unsigned int id, unsigned long rate)
{
	unsigned long fin = REFCLK_RATE, fout = rate;
	uint8_t min_refdiv, max_refdiv, postdiv, div1, div2;
	uint8_t dsmpd = 1, postdiv1 = 0, postdiv2 = 0, refdiv = 0;
	uint16_t fbdiv = 0;
	uint32_t frac = 0;
	uint64_t foutvco, foutpostdiv;
	uint32_t reg;

	/*
	 * FREF : 10MHz ~ 800MHz
	 * FREFDIV : 1MHz ~ 40MHz
	 * FOUTVCO : 400MHz ~ 1.6GHz
	 * FOUTPOSTDIV : 8MHz ~ 1.6GHz
	 */
	if (fin < MIN_FREF_RATE || fin > MAX_FREF_RATE)
		return 0;

	if (fout < MIN_FOUTPOSTDIV_RATE || fout > MAX_FOUTPOSTDIV_RATE)
		return 0;

	if (id == CGU_CLK_CPLL)
		reg = CRU_CPLL_CON00;
	else
		reg = CRU_GPLL_CON00;

	if (fin == fout) {
		cru_update_bits(reg + CRU_CPLL_CON00,
				PLL_BYPASS_MASK, PLL_BYPASS);
		cru_update_bits(reg + CRU_CPLL_CON01, PLL_PD_MASK, 0);
		while (!(cru_read(reg + CRU_CPLL_CON01) & PLL_LOCK));
		return fin;
	}

	min_refdiv = fin / MAX_FREFDIV_RATE + 1;
	max_refdiv = fin / MIN_FREFDIV_RATE;
	if (max_refdiv > 64)
		max_refdiv = 64;

	if (fout < MIN_FVCO_RATE) {
		div1 = DIV_ROUND_UP(MIN_FVCO_RATE, fout);
		div2 = DIV_ROUND_UP(MAX_FVCO_RATE, fout);
		for (postdiv = div1; postdiv <= div2; postdiv++) {
			/* fix prime number that can not find right div*/
			for (postdiv2 = 1; postdiv2 < 8; postdiv2++) {
				if (postdiv % postdiv2)
					continue;

				postdiv1 = postdiv / postdiv2;

				if (postdiv1 > 0 && postdiv1 < 8)
					break;
			}
			if (postdiv2 > 7) {
				continue;
			} else {
				break;
			}
		}
		if (postdiv > div2) {
			return 0;
		}
		fout *= postdiv1 * postdiv2;
	} else {
		postdiv1 = 1;
		postdiv2 = 1;
	}
	for (refdiv = min_refdiv; refdiv <= max_refdiv; refdiv++) {
		uint64_t tmp, frac_rate;

		if (fin % refdiv)
			continue;

		tmp = (uint64_t)fout * refdiv;
		do_div(tmp, fin);
		fbdiv = tmp;
		if (fbdiv < 10 || fbdiv > 1600)
			continue;

		tmp = (uint64_t)fbdiv * fin;
		do_div(tmp, refdiv);
		if (fout < MIN_FVCO_RATE || fout > MAX_FVCO_RATE)
			continue;

		frac_rate = fout - tmp;

		if (frac_rate) {
			tmp = (uint64_t)frac_rate * refdiv;
			tmp <<= 24;
			do_div(tmp, fin);
			frac = tmp;
			dsmpd = 0;
		}

		break;
	}

	/*
	 * If DSMPD = 1 (DSM is disabled, "integer mode")
	 * FOUTVCO = FREF / REFDIV * FBDIV
	 * FOUTPOSTDIV = FOUTVCO / POSTDIV1 / POSTDIV2
	 *
	 * If DSMPD = 0 (DSM is enabled, "fractional mode")
	 * FOUTVCO = FREF / REFDIV * (FBDIV + FRAC / 2^24)
	 * FOUTPOSTDIV = FOUTVCO / POSTDIV1 / POSTDIV2
	 */
	foutvco = fin * fbdiv;
	do_div(foutvco, refdiv);

	if (!dsmpd) {
		uint64_t frac_rate = (uint64_t)fin * frac;

		do_div(frac_rate, refdiv);
		foutvco += frac_rate >> 24;
	}

	foutpostdiv = foutvco;
	do_div(foutpostdiv, postdiv1);
	do_div(foutpostdiv, postdiv2);

	cru_update_bits(reg + CRU_CPLL_CON00,
			PLL_BYPASS_MASK | PLL_POSTDIV1_MASK | PLL_FBDIV_MASK,
			PLL_POSTDIV1(postdiv1) | PLL_FBDIV(fbdiv));
	cru_update_bits(reg + CRU_CPLL_CON01,
			PLL_DSMPD_MASK | PLL_POSTDIV2_MASK | PLL_REFDIV_MASK,
			PLL_DSMPD(dsmpd) | PLL_POSTDIV2(postdiv2) | PLL_REFDIV(refdiv));
	cru_write(reg + CRU_CPLL_CON02, PLL_FRAC(frac));
	while (!(cru_read(reg + CRU_CPLL_CON01) & PLL_LOCK));

	return (unsigned long)foutpostdiv;
}

static unsigned long cru_clk_set_rate_sclk_vop(unsigned long rate)
{
	unsigned long m, n, parent_rate;
	uint32_t val;

	val = cru_read(CRU_CLKSEL_CON02) & SCLK_VOP_SEL_MASK;
	if (val == SCLK_VOP_SEL_GPLL)
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_GPLL);
	else
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_CPLL);

	rational_best_approximation(rate, parent_rate,
				    GENMASK(15, 0), GENMASK(15, 0),
				    &m, &n);
	cru_write(CRU_CLKSEL_CON13, m << 16 | n);

	return rate;
}

static unsigned long cru_clk_set_rate_sclk_hdmirx_aud(unsigned long rate)
{
	uint64_t parent_rate;
	uint8_t div;

	parent_rate = cru_clk_set_rate_pll(CGU_CLK_GPLL, rate*4);
	div = DIV_ROUND_CLOSEST_ULL(parent_rate, rate);
	do_div(parent_rate, div);
	rate = parent_rate;
	cru_write(CRU_CLKSEL_CON05, 0x3fc0 << 16 | ((div - 1) << 6) |
		  CLK_HDMIRX_AUD_SEL << 16 | CLK_HDMIRX_AUD_SEL );
	return rate;
}

static unsigned long cru_clk_get_rate_sclk_hdmirx_aud(void)
{
	unsigned long rate;
	uint64_t parent_rate;
	uint8_t div;
	uint32_t val;

	val = cru_read(CRU_CLKSEL_CON05);
	if (val & CLK_HDMIRX_AUD_SEL)
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_GPLL);
	else
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_CPLL);
	div = ((val&0x3fc0) >> 6) + 1;
	do_div(parent_rate, div);
	rate = parent_rate;
	return rate;
}

static unsigned long cru_clk_set_rate_rx_read(unsigned long rate)
{
	unsigned long m, n, parent_rate;
	uint32_t val;

	val = cru_read(CRU_CLKSEL_CON02) & CLK_RX_READ_SEL_MASK;
	if (val == CLK_RX_READ_SEL_GPLL)
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_GPLL);
	else
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_CPLL);

	rational_best_approximation(rate, parent_rate,
				    GENMASK(15, 0), GENMASK(15, 0),
				    &m, &n);
	cru_write(CRU_CLKSEL_CON14, m << 16 | n);

	return rate;
}

static unsigned long cru_clk_get_rate_uart_src(void)
{
	unsigned long rate, parent_rate;
	uint32_t mux, div;

	mux = cru_read(CRU_CLKSEL_CON21) & CLK_UART_SRC_SEL_MASK;
	if (mux == CLK_UART_SRC_SEL_GPLL)
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_GPLL);
	else
		parent_rate = cru_clk_get_rate_pll(CGU_CLK_CPLL);

	div = cru_read(CRU_CLKSEL_CON21) & CLK_UART_SRC_DIV_MASK;
	div >>= CLK_UART_SRC_DIV_SHIFT;
	rate = parent_rate / (div + 1);

	return rate;
}

static unsigned long cru_clk_set_rate_sclk_uart(unsigned long rate)
{
	unsigned long m, n, parent_rate;

	parent_rate = cru_clk_get_rate_uart_src();

	if (rate == REFCLK_RATE) {
		cru_update_bits(CRU_CLKSEL_CON06, SCLK_UART_SEL_MASK,
				SCLK_UART_SEL_OSC);
		return rate;
	} else if (rate == parent_rate) {
		cru_update_bits(CRU_CLKSEL_CON06, SCLK_UART_SEL_MASK,
				SCLK_UART_SEL_UART_SRC);
		return rate;
	} else {
		cru_update_bits(CRU_CLKSEL_CON06, SCLK_UART_SEL_MASK,
				SCLK_UART_SEL_UART_FRAC);
	}

	rational_best_approximation(rate, parent_rate,
				    GENMASK(15, 0), GENMASK(15, 0),
				    &m, &n);
	cru_write(CRU_CLKSEL_CON20, m << 16 | n);

	return rate;
}

int rk628_cru_clk_set_rate(unsigned int id, unsigned long rate)
{
	switch (id) {
	case CGU_CLK_CPLL:
	case CGU_CLK_GPLL:
		cru_clk_set_rate_pll(id, rate);
		break;
	case CGU_CLK_RX_READ:
		cru_clk_set_rate_rx_read(rate);
		break;
	case CGU_SCLK_VOP:
		cru_clk_set_rate_sclk_vop(rate);
		break;
	case CGU_SCLK_UART:
		cru_clk_set_rate_sclk_uart(rate);
		break;
	case CGU_CLK_HDMIRX_AUD:
		cru_clk_set_rate_sclk_hdmirx_aud(rate);
		break;
	default:
		return -1;
	}

	return 0;
}

unsigned long rk628_cru_clk_get_rate(unsigned int id)
{
	unsigned long rate;

	switch (id) {
	case CGU_CLK_CPLL:
	case CGU_CLK_GPLL:
		rate = cru_clk_get_rate_pll(id);
		break;
	case CGU_CLK_HDMIRX_AUD:
		rate = cru_clk_get_rate_sclk_hdmirx_aud();
		break;
	default:
		return 0;
	}

	return rate;
}

void rk628_cru_init(void)
{
	//cru_update_bits(CRU_MODE_CON00, CLK_GPLL_MODE_MASK | CLK_CPLL_MODE_MASK,
	//		CLK_GPLL_MODE_GPLL | CLK_CPLL_MODE_CPLL);
#if 1
	cru_write(CRU_GPLL_CON00, 0xffff701d);
	msleep(1);
	cru_write(CRU_MODE_CON00, 0xffff0004);
	msleep(1);
	cru_write(CRU_CLKSEL_CON00, 0x00ff0080);
	cru_write(CRU_CLKSEL_CON00, 0x00ff0083);
	cru_write(CRU_CPLL_CON00, 0xffff3063);
	msleep(1);
	cru_write(CRU_MODE_CON00, 0xffff0005);
	cru_write(CRU_CLKSEL_CON00, 0x00ff0003);
	cru_write(CRU_CLKSEL_CON00, 0x00ff000b);
	cru_write(CRU_GPLL_CON00, 0xffff1028);
	msleep(1);
	cru_write(CRU_CLKSEL_CON00, 0x00ff008b);
	cru_write(CRU_CPLL_CON00, 0xffff1063);
	msleep(1);
	cru_write(CRU_CLKSEL_CON00, 0x00ff000b);
#endif
}

MODULE_AUTHOR("Shunqing Chen <csq@rock-chips.com>");
MODULE_DESCRIPTION("Rockchip RK628 CRU driver");
MODULE_LICENSE("GPL v2");
